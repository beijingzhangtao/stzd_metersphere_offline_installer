#! /bin/sh

if [[ -d ../extras/metersphere-offline-installer-v1.20.8-lts ]]; then
  # 把官网下载的离线安装包里的docker目录和images目录复制过来
  echo "复制官方离线一键安装包里的docker目录和images目录到本项目......."
  \cp -r ../extras/metersphere-offline-installer-v1.20.8-lts/docker .
  \cp -r ../extras/metersphere-offline-installer-v1.20.8-lts/images .
  \rm images/metersphere.tar
else
  echo "因为官方安装包的文件有点大，所以不提交到本代码库了，制作我们自己的离线安装包时从docker目录和images里拷贝过来替换我们修改过的就可以了"
  echo "请下载好官方的离线一键安装包metersphere-offline-installer-v1.20.8-lts.tar.gz, 并且解压到本文件的上级目录"
  echo "用ls ../extras/metersphere-offline-installer-v1.20.8-lts检查可以看到有docker目录和images目录"
  exit
fi

# 安装并启动docker
if which docker >/dev/null; then
  echo "检测到 Docker 已安装，跳过安装步骤"
  echo "启动 Docker "
  service docker start
else
  echo "... 离线安装 docker"
  cp docker/bin/* /usr/bin/
  cp docker/service/docker.service /etc/systemd/system/
  chmod +x /usr/bin/docker*
  chmod 754 /etc/systemd/system/docker.service
  echo "... 启动 docker"
  service docker start
fi


echo "登陆到gitlab容器镜像仓库"
# 如果提示权限问题可能是没有登陆，先 docker login, 如需退出 docker logout registry.gitlab.com
echo glpat-Xmb-cww6BzhG61foADXw | docker login registry.gitlab.com -u df1228@gmail.com --password-stdin

echo "拉取定制过的镜像并复制到或替换images目录下相应的镜像"
# 定制过的metershpere 主镜像
docker images -f "dangling=true" -aq | xargs docker rmi -f
# docker rmi registry.cn-qingdao.aliyuncs.com/metersphere/metersphere:v1.20.8-lts
# docker rmi registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:latest

docker pull registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:latest
docker tag registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:latest registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:v1.20.8-lts
docker save registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:v1.20.8-lts -o ./images/metersphere.tar
# docker tag registry.gitlab.com/beijingzhangtao/sztd_metersphere/metersphere:latest registry.cn-qingdao.aliyuncs.com/metersphere/metersphere:v1.20.8-lts
# docker save registry.cn-qingdao.aliyuncs.com/metersphere/metersphere:v1.20.8-lts -o ./images/metersphere.tar

# 菜单1
docker pull registry.gitlab.com/beijingzhangtao/sztd_ui_test:latest
docker save registry.gitlab.com/beijingzhangtao/sztd_ui_test:latest -o ./images/sztd_ui_test.tar

# 菜单2
docker pull registry.gitlab.com/beijingzhangtao/sztd_jenkins_ci_cd/backend:latest
docker save registry.gitlab.com/beijingzhangtao/sztd_jenkins_ci_cd/backend:latest -o ./images/sztd_jenkins_ci_cd_backend.tar
docker pull registry.gitlab.com/beijingzhangtao/sztd_jenkins_ci_cd/frontend:latest
docker save registry.gitlab.com/beijingzhangtao/sztd_jenkins_ci_cd/frontend:latest -o ./images/sztd_jenkins_ci_cd_frontend.tar

# jenkins 镜像
docker pull jenkinsci/blueocean:1.25.5
docker save jenkinsci/blueocean:1.25.5 -o images/jenkins.tar


# sonarqube 镜像
## execute this on docker host, or sonarqube will exit on bootstrap because of elasticsearch
sysctl -w vm.max_map_count=262144

# https://github.com/SonarSource/docker-sonarqube/issues/282
# https://community.sonarsource.com/t/error-elasticsearch-did-not-exit-normally-check-the-logs-at-opt-sonarqube-logs-sonarqube-log-due-low-value-of-vm-max-map-count/40117
# https://community.sonarsource.com/t/sonarqube-will-not-start-in-docker-container/56250/2
docker pull sonarqube:8.9.9-community
docker save sonarqube:8.9.9-community -o images/sonarqube.tar
docker pull postgres:12
docker save postgres -o images/postgres.tar

# jmeter镜像
# docker pull justb4/jmeter
# docker save justb4/jmeter -o images/jmeter.tar

# 如果需要运行jmeter命令
# docker run --rm justb4/jmeter

# junit镜像

echo "请手动运行一键安装脚本吧"
echo "./install.sh"
