# MeterSphere 离线一键安装包

TLDR: 运行 ./gen.sh 生成离线一键安装包

此安装包是基于v1.20.8-lts修改而来的。 和5个多G的metersphere-offline-installer-v1.20.8-lts.tar相比，少了docker和images目录。

docker目录是用于离线环境下安装docker和docker-compose的二进制文件。
images目录是用docker save导出的镜像压缩包，一键安装的时候脚本会自动load所有镜像到docker里。

所以将原有的images/metersphere.tar替换为我们修改过后重新打包的metersphere.tar
我们将docker镜像的tag改为一样的，直接替换就好了。

另外我们基于metersphere/metersphere二次开发，加了两个菜单，都打包了另外的镜像，也要整合进来。

metersphere目录里加了两个docker-compose file，分别是docker-compose-ui-test.yml 和 docker-compose-jenkins.yml，对应的是菜单1和菜单2。

运行./install.sh的时候会自动复制到/opt/meterspher，具体逻辑可以查看msctl脚本，所以如果想要加自定义模块到一键安装，可以复制docker-compose file到metersphere文件夹，并且修改msctl即可。

运行./gen.sh能够自动生成一键离线安装包。由于docker目录和images目录太大所以不提交到git仓库了，将下载好的metersphere-offline-installer-v1.20.8-lts.tar.gz解压，放到./gen.sh的上级目录。运行gen.sh会自动拷贝。



beijingzhangtao/sztd_metersphere_offline_installer: 生成离线安装包的项目

beijingzhangtao/sztd_metersphere: metersphere项目源码，加了两个子菜单: 一个是自动化，一个是UI测试。

beijingzhangtao/sztd_jenkins_ci_cd: 自动化子菜单

beijingzhangtao/sztd_jenkins_ui_test: UI测试子菜单



### 本地更新了代码之后如何更新到离线环境 ？

首先提交了代码之后，线上会自动跑流水线，如果没有失败，会生成一个新的镜像，这个镜像就是你打包的最新的，需要导出为压缩文件刻盘弄到机房去。（docker save -o）， 弄到机房去之后替换images/下的同名文件。
docker load -i 该文件可以把镜像压缩包导入到docker中，可以用docker images来验证是否导入了最新的镜像。
然后就可以用新的镜像重启容器了。 msctl up -d --force-recreate xxxxx (替换为服务名，一般为ms-server、ui-test、jenkins-frontend jenkins-backend)

或者直接重建msctl up -d --force-recreate ms-server ui-test jenkins-frontend jenkins-backend
或者干脆重建所有容器都行 msctl up -d --force-recreate 


把 metersphere-offline-installer-v1.20.8-lts.tar.gz 和 jdk、maven 都放到extras目录里去吧。 
和sztd_meterspher_offline_installer平级即可，metersphere-offline-installer-v1.20.8-lts.tar.gz需要提前解压。

