-- DROP DATABASE IF EXISTS `auto_ui`;
-- CREATE DATABASE `auto_ui`;
-- USE `auto_ui`;

/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.8-myslq5.7-ok
Source Server Version : 50738
Source Host           : 192.168.1.8:3309
Source Database       : auto_ui

Target Server Type    : MYSQL
Target Server Version : 50738
File Encoding         : 65001

Date: 2022-06-23 22:50:31
*/

-- SET NAMES utf8;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `flyway_schema_history`
-- ----------------------------
DROP TABLE IF EXISTS `flyway_schema_history`;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of flyway_schema_history
-- ----------------------------
INSERT INTO `flyway_schema_history` VALUES ('1', '3.1.1', 'BaseLineInitialize', 'SQL', 'V3.1.1__BaseLineInitialize.sql', '-163164068', 'root', '2022-05-13 17:31:29', '1134', '1');
INSERT INTO `flyway_schema_history` VALUES ('2', '3.2', 'upgradeData', 'SQL', 'V3.2__upgradeData.sql', '398406771', 'root', '2022-05-13 17:31:30', '95', '1');
INSERT INTO `flyway_schema_history` VALUES ('3', '3.2.1', 'upgradeData', 'SQL', 'V3.2.1__upgradeData.sql', '1916738803', 'root', '2022-05-13 17:31:30', '76', '1');
INSERT INTO `flyway_schema_history` VALUES ('4', '3.3', 'upgradeData', 'SQL', 'V3.3__upgradeData.sql', '-1434555367', 'root', '2022-05-13 17:31:30', '55', '1');
INSERT INTO `flyway_schema_history` VALUES ('5', '3.5', 'upgradeData', 'SQL', 'V3.5__upgradeData.sql', '1935020972', 'root', '2022-05-13 17:31:30', '204', '1');

-- ----------------------------
-- Table structure for `project_case`
-- ----------------------------
DROP TABLE IF EXISTS `project_case`;
CREATE TABLE `project_case` (
  `case_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '测试用例ID',
  `case_serial_number` int(8) NOT NULL COMMENT '用例编号排序',
  `case_sign` varchar(20) NOT NULL COMMENT '用例标识',
  `case_name` varchar(200) NOT NULL COMMENT '用例名称',
  `project_id` int(8) NOT NULL COMMENT '关联项目ID',
  `module_id` int(8) NOT NULL COMMENT '关联项目模块ID',
  `case_type` int(2) NOT NULL COMMENT '默认类型 0 HTTP接口 1 Web UI 2 API驱动  3移动端',
  `failcontinue` int(2) DEFAULT '0' COMMENT '前置步骤失败，后续步骤是否继续，0：中断，1：继续',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`case_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='项目测试用例管理';

-- ----------------------------
-- Records of project_case
-- ----------------------------
INSERT INTO `project_case` VALUES ('1', '1', '1001-1', '登录测试用例', '2', '2', '1', '0', 'admin', '2022-05-13 17:42:45', 'admin', '2022-05-15 11:55:46', 'web ui 测试');
INSERT INTO `project_case` VALUES ('3', '2', '1001-2', '测试新增001', '2', '2', '0', '0', 'admin', '2022-06-20 09:40:14', 'admin', '2022-06-20 09:44:04', 'ui系统01');
INSERT INTO `project_case` VALUES ('8', '3', '1001-3', 'Copy【测试新增001】', '2', '2', '0', '0', 'admin', '2022-06-20 11:26:42', 'admin', '2022-06-20 11:26:42', 'ui系统01');

-- ----------------------------
-- Table structure for `project_case_debug`
-- ----------------------------
DROP TABLE IF EXISTS `project_case_debug`;
CREATE TABLE `project_case_debug` (
  `debug_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '调试ID',
  `case_id` int(8) NOT NULL COMMENT '用例ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `debug_isend` int(2) NOT NULL COMMENT '调试结束标识 0 进行中 1结束 2异常',
  `log_level` varchar(10) NOT NULL COMMENT '日志级别 info 记录 warning 警告 error 异常',
  `log_detail` varchar(5000) NOT NULL COMMENT '日志',
  PRIMARY KEY (`debug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用例调试日志记录';

-- ----------------------------
-- Records of project_case_debug
-- ----------------------------

-- ----------------------------
-- Table structure for `project_case_module`
-- ----------------------------
DROP TABLE IF EXISTS `project_case_module`;
CREATE TABLE `project_case_module` (
  `module_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '模块ID',
  `module_name` varchar(50) DEFAULT NULL COMMENT '模块名称',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `parent_id` int(11) DEFAULT '0' COMMENT '父模块id',
  `ancestors` varchar(100) DEFAULT '' COMMENT '祖模块列表',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='测试用例模块管理';

-- ----------------------------
-- Records of project_case_module
-- ----------------------------
INSERT INTO `project_case_module` VALUES ('1', '测试项目一', '1', '0', '0', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '初始化数据');
INSERT INTO `project_case_module` VALUES ('2', '海军作战应急项目', '2', '0', '0', '1', 'admin', '2022-05-13 17:42:19', 'admin', '2022-05-13 17:42:19', '项目初始化模块');

-- ----------------------------
-- Table structure for `project_case_params`
-- ----------------------------
DROP TABLE IF EXISTS `project_case_params`;
CREATE TABLE `project_case_params` (
  `params_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '用例参数ID',
  `params_name` varchar(50) NOT NULL COMMENT '参数名称',
  `params_value` varchar(500) NOT NULL COMMENT '参数值',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `env_name` varchar(255) DEFAULT NULL COMMENT '测试环境',
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用例公共参数';

-- ----------------------------
-- Records of project_case_params
-- ----------------------------

-- ----------------------------
-- Table structure for `project_case_steps`
-- ----------------------------
DROP TABLE IF EXISTS `project_case_steps`;
CREATE TABLE `project_case_steps` (
  `step_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '步骤ID',
  `case_id` int(8) NOT NULL COMMENT '用例ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `step_serial_number` int(2) NOT NULL COMMENT '步骤序号',
  `step_path` varchar(500) DEFAULT NULL COMMENT '包路径|定位路径',
  `step_operation` varchar(100) DEFAULT NULL COMMENT '方法名|操作',
  `step_parameters` varchar(500) DEFAULT NULL COMMENT '参数',
  `action` varchar(50) DEFAULT NULL COMMENT '步骤动作',
  `expected_result` varchar(2000) DEFAULT NULL COMMENT '预期结果',
  `step_type` int(2) NOT NULL COMMENT '默认类型 0 HTTP接口 1 Web UI 2 API驱动  3移动端',
  `extend` varchar(200) DEFAULT NULL COMMENT '扩展字段，可用于备注、存储HTTP模板等',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `step_remark` varchar(200) DEFAULT NULL COMMENT '备注字段，给接口类型的用例步骤使用',
  PRIMARY KEY (`step_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='测试用例步骤管理';

-- ----------------------------
-- Records of project_case_steps
-- ----------------------------
INSERT INTO `project_case_steps` VALUES ('1', '1', '2', '1', null, 'open', 'https://www.baidu.com/', null, null, '1', null, 'admin', '2022-05-15 11:55:46', 'admin', '2022-05-15 11:55:46', '备注');

-- ----------------------------
-- Table structure for `project_plan`
-- ----------------------------
DROP TABLE IF EXISTS `project_plan`;
CREATE TABLE `project_plan` (
  `plan_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '测试计划ID',
  `plan_name` varchar(50) NOT NULL COMMENT '测试计划名称',
  `plan_case_count` int(8) DEFAULT NULL COMMENT '计划中用例总数',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试计划';

-- ----------------------------
-- Records of project_plan
-- ----------------------------

-- ----------------------------
-- Table structure for `project_plan_case`
-- ----------------------------
DROP TABLE IF EXISTS `project_plan_case`;
CREATE TABLE `project_plan_case` (
  `plan_case_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '计划用例ID',
  `case_id` int(8) NOT NULL COMMENT '用例ID',
  `plan_id` int(8) NOT NULL COMMENT '测试计划ID',
  `priority` int(8) NOT NULL COMMENT '用例优先级 数字越小，优先级越高',
  PRIMARY KEY (`plan_case_id`),
  KEY `case_id` (`case_id`) USING BTREE,
  KEY `plan_id` (`plan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试计划用例';

-- ----------------------------
-- Records of project_plan_case
-- ----------------------------

-- ----------------------------
-- Table structure for `project_protocol_template`
-- ----------------------------
DROP TABLE IF EXISTS `project_protocol_template`;
CREATE TABLE `project_protocol_template` (
  `template_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `template_name` varchar(50) NOT NULL COMMENT '模板名称',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `head_msg` varchar(3000) DEFAULT NULL COMMENT '消息头',
  `cerificate_path` varchar(300) DEFAULT NULL COMMENT '客户端中的证书路径',
  `encoding` varchar(20) NOT NULL COMMENT '编码格式',
  `timeout` int(8) NOT NULL COMMENT '超时时间',
  `is_response_head` int(2) NOT NULL COMMENT '请求响应返回值是否带头域信息 0不带 1带',
  `is_response_code` int(2) NOT NULL COMMENT '请求响应返回值是否带状态码 0不带 1带',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='协议模板管理';

-- ----------------------------
-- Records of project_protocol_template
-- ----------------------------

-- ----------------------------
-- Table structure for `project_suite`
-- ----------------------------
DROP TABLE IF EXISTS `project_suite`;
CREATE TABLE `project_suite` (
  `suite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '聚合计划ID',
  `suite_name` varchar(50) NOT NULL COMMENT '聚合计划名称',
  `suite_plan_count` int(11) DEFAULT NULL COMMENT '聚合计划中的计划总数',
  `project_id` int(11) NOT NULL COMMENT '项目ID',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`suite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聚合计划';

-- ----------------------------
-- Records of project_suite
-- ----------------------------

-- ----------------------------
-- Table structure for `project_suite_plan`
-- ----------------------------
DROP TABLE IF EXISTS `project_suite_plan`;
CREATE TABLE `project_suite_plan` (
  `suite_plan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '聚合计划ID',
  `suite_id` int(11) NOT NULL COMMENT '聚合ID',
  `plan_id` int(11) NOT NULL COMMENT '测试计划ID',
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`suite_plan_id`),
  KEY `suite_id` (`suite_id`) USING BTREE,
  KEY `plan_id` (`plan_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聚合计划';

-- ----------------------------
-- Records of project_suite_plan
-- ----------------------------

-- ----------------------------
-- Table structure for `project_template_params`
-- ----------------------------
DROP TABLE IF EXISTS `project_template_params`;
CREATE TABLE `project_template_params` (
  `params_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '模板参数ID',
  `template_id` int(8) NOT NULL COMMENT '模板ID',
  `param_name` varchar(50) NOT NULL COMMENT '参数名',
  `param_value` varchar(5000) DEFAULT NULL COMMENT '参数默认值',
  `param_type` int(4) NOT NULL COMMENT '0 String 1 JSON对象 2 JSONARR对象 3 文件类型 4数字类型 5 布尔类型',
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板参数管理';

-- ----------------------------
-- Records of project_template_params
-- ----------------------------

-- ----------------------------
-- Table structure for `qa_accident`
-- ----------------------------
DROP TABLE IF EXISTS `qa_accident`;
CREATE TABLE `qa_accident` (
  `accident_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '事故ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `accident_status` varchar(80) NOT NULL COMMENT '事故状态',
  `accident_time` varchar(64) DEFAULT NULL COMMENT '事故发生时间',
  `report_time` varchar(64) NOT NULL COMMENT '事故报告时间',
  `accident_description` varchar(500) NOT NULL COMMENT '事故描述',
  `accident_level` varchar(60) NOT NULL COMMENT '事故等级',
  `accident_analysis` varchar(500) DEFAULT NULL COMMENT '事故分析',
  `accident_type` varchar(100) DEFAULT NULL COMMENT '事故类型',
  `accident_consequence` varchar(300) DEFAULT NULL COMMENT '事故影响后果',
  `corrective_action` varchar(300) DEFAULT NULL COMMENT '纠正措施',
  `resolution_time` varchar(64) DEFAULT NULL COMMENT '解决时间',
  `resolutioner` varchar(20) DEFAULT NULL COMMENT '解决处理人',
  `preventive_action` varchar(300) DEFAULT NULL COMMENT '预防措施',
  `preventiver` varchar(20) DEFAULT NULL COMMENT '预防措施责任人',
  `preventive_plan_date` varchar(64) DEFAULT NULL COMMENT '预防措施计划完成时间',
  `preventive_over_date` varchar(64) DEFAULT NULL COMMENT '预防措施实际完成时间',
  `duration_time` int(8) DEFAULT NULL COMMENT '事故持续时间',
  `impact_time` int(8) DEFAULT NULL COMMENT '事故影响时间',
  `accident_file_name` varchar(100) DEFAULT NULL COMMENT '事故报告附件路径',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`accident_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生产事故登记';

-- ----------------------------
-- Records of qa_accident
-- ----------------------------

-- ----------------------------
-- Table structure for `qa_version`
-- ----------------------------
DROP TABLE IF EXISTS `qa_version`;
CREATE TABLE `qa_version` (
  `version_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '版本ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `version_number` varchar(40) DEFAULT NULL COMMENT '版本号',
  `version_status` varchar(40) DEFAULT NULL COMMENT '版本状态',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `developer` varchar(40) DEFAULT NULL COMMENT '开发人员',
  `tester` varchar(40) DEFAULT NULL COMMENT '测试人员',
  `plan_finish_date` varchar(20) DEFAULT NULL COMMENT '计划完成日期',
  `actually_finish_date` varchar(20) DEFAULT NULL COMMENT '实际完成日期',
  `launch_date` varchar(20) DEFAULT NULL COMMENT '上线日期',
  `time_limit_version` int(4) DEFAULT NULL COMMENT '版本工期 单位:天',
  `project_deviation_days` int(4) DEFAULT NULL COMMENT '项目偏移时间 单位:天',
  `project_deviation_percent` float DEFAULT '0' COMMENT '项目偏移百分比',
  `demand_plan_finish` int(4) DEFAULT NULL COMMENT '计划完成需求数',
  `demand_actually_finish` int(4) DEFAULT NULL COMMENT '实际完成需求数',
  `demand_percent` float DEFAULT '0' COMMENT '需求完成百分比',
  `testcase_count` int(8) DEFAULT NULL COMMENT '测试用例数',
  `testing_return` int(2) DEFAULT NULL COMMENT '转测试打回次数',
  `dev_human_resources` int(8) DEFAULT NULL COMMENT '开发投入人力 单位:人/天',
  `test_human_resources` int(8) DEFAULT NULL COMMENT '测试投入人力 单位:人/天',
  `change_line_code` int(8) DEFAULT NULL COMMENT '代码变动行数',
  `bug_zm` int(4) DEFAULT NULL COMMENT 'Bug数量 级别:致命',
  `bug_yz` int(4) DEFAULT NULL COMMENT 'Bug数量 级别:严重',
  `bug_yb` int(4) DEFAULT NULL COMMENT 'Bug数量 级别:一般',
  `bug_ts` int(4) DEFAULT NULL COMMENT 'Bug数量 级别:提示',
  `code_di` float DEFAULT NULL COMMENT '代码DI值',
  `quality_review` varchar(1000) DEFAULT NULL COMMENT '质量回溯',
  `imprint` varchar(1000) DEFAULT NULL COMMENT '版本说明',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='质量管理-版本管理';

-- ----------------------------
-- Records of qa_version
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('LuckyFrameScheduler', '__TASK_CLASS_NAME__100', 'DEFAULT', '0/30 * * * * ? ', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------
INSERT INTO `qrtz_fired_triggers` VALUES ('LuckyFrameScheduler', 'PE8S46WQX0H3MZ116559082589411655908259056', '__TASK_CLASS_NAME__100', 'DEFAULT', 'PE8S46WQX0H3MZ11655908258941', '1655912371131', '1655912400000', '5', 'ACQUIRED', null, null, '0', '0');

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('LuckyFrameScheduler', '__TASK_CLASS_NAME__100', 'DEFAULT', null, 'com.luckyframe.project.monitor.job.util.ScheduleJob', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372002D636F6D2E6C75636B796672616D652E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002E636F6D2E6C75636B796672616D652E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000180BCF3F2A178707400007071007E000E7371007E000F770800000180BCF3F2A17874000F302F3330202A202A202A202A203F20740015E5AEA2E688B7E7ABAFE5BF83E8B7B3E79B91E590AC7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000006474000B636C69656E74486561727474000968656172745461736B7400093132372E302E302E3174000133740001307800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('LuckyFrameScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('LuckyFrameScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('LuckyFrameScheduler', 'PE8S46WQX0H3MZ11655908258941', '1655912359770', '15000');

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('LuckyFrameScheduler', '__TASK_CLASS_NAME__100', 'DEFAULT', '__TASK_CLASS_NAME__100', 'DEFAULT', null, '1655912400000', '1655912370000', '5', 'ACQUIRED', 'CRON', '1652437545000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372002D636F6D2E6C75636B796672616D652E70726F6A6563742E6D6F6E69746F722E6A6F622E646F6D61696E2E4A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E00097872002E636F6D2E6C75636B796672616D652E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000180BCF3F028787074000070707074000F302F3330202A202A202A202A203F20740015E5AEA2E688B7E7ABAFE5BF83E8B7B3E79B91E590AC7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000006474000B636C69656E74486561727474000968656172745461736B7400093132372E302E302E3174000133740001307800);

-- ----------------------------
-- Table structure for `sys_client`
-- ----------------------------
DROP TABLE IF EXISTS `sys_client`;
CREATE TABLE `sys_client` (
  `client_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '客户端ID',
  `job_id` int(8) DEFAULT NULL COMMENT '心跳任务ID',
  `client_name` varchar(30) NOT NULL COMMENT '客户端名称',
  `client_ip` varchar(30) NOT NULL COMMENT '客户端IP',
  `status` int(2) DEFAULT NULL COMMENT '客户端状态 0 正常 1 链接失败 2 状态未知',
  `checkinterval` int(6) NOT NULL COMMENT '检查客户端状态心跳间隔时间 单位:秒',
  `client_path` varchar(500) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `client_type` int(2) DEFAULT '0' COMMENT '客户端链接类型 0 普通HTTP 1 NETTY通信',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='客户端管理表';

-- ----------------------------
-- Records of sys_client
-- ----------------------------
INSERT INTO `sys_client` VALUES ('10', null, '测试客户端', '192.168.0.104', '1', '30', '/TestDriven', '服务端启动重新初始化', '1');
INSERT INTO `sys_client` VALUES ('11', '100', 'sztd-ui', '127.0.0.1', '1', '30', '/TestDriven;/BrowserDriven', '检测客户端远程异常', '0');
INSERT INTO `sys_client` VALUES ('12', null, '测试客户端', '192.168.0.103', '1', '30', '/TestDriven', '服务端启动重新初始化', '1');
INSERT INTO `sys_client` VALUES ('13', null, '测试客户端', '192.168.0.107', '1', '30', '/TestDriven', '服务端启动重新初始化', '1');

-- ----------------------------
-- Table structure for `sys_client_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_client_config`;
CREATE TABLE `sys_client_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `client_id` int(8) NOT NULL COMMENT '客户端id',
  `config_key` varchar(100) NOT NULL COMMENT '客户端配置名称',
  `config_value` varchar(400) NOT NULL COMMENT '客户端配置值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='客户端配置表';

-- ----------------------------
-- Records of sys_client_config
-- ----------------------------
INSERT INTO `sys_client_config` VALUES ('6', '11', 'g', 'C:\\Users\\MI\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe');
INSERT INTO `sys_client_config` VALUES ('7', '13', '测试配置01', '20');

-- ----------------------------
-- Table structure for `sys_client_project`
-- ----------------------------
DROP TABLE IF EXISTS `sys_client_project`;
CREATE TABLE `sys_client_project` (
  `client_id` int(8) NOT NULL COMMENT '客户端ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  PRIMARY KEY (`client_id`,`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端与项目关联表';

-- ----------------------------
-- Records of sys_client_project
-- ----------------------------
INSERT INTO `sys_client_project` VALUES ('11', '1');
INSERT INTO `sys_client_project` VALUES ('11', '2');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(100) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-purple', 'Y', 'admin', '2019-02-13 10:27:32', 'admin', '2022-05-13 17:56:36', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '初始化密码 123456');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` int(11) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '航天智信-总公司', '0', 'zhangtao', '13311273513', '393877568@qq.com', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2022-05-13 18:25:17');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '深圳分公司', '1', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '长沙分公司', '2', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '研发部门', '1', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '测试部门', '3', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '运维部门', '5', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('106', '102', '0,100,102', '测试部门', '1', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('107', '102', '0,100,102', '研发部门', '2', 'luckyframe', '15888888888', 'admin@luckyframe.cn', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32');
INSERT INTO `sys_dept` VALUES ('108', '100', '0,100', '上路孤儿', '6', '白不醒', '18871005653', '891368884@qq.com', '0', '0', 'admin', '2022-06-20 03:16:48', '', null);

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3042 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通知');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '公告');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '新增操作');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '修改操作');
INSERT INTO `sys_dict_data` VALUES ('18', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '删除操作');
INSERT INTO `sys_dict_data` VALUES ('19', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '授权操作');
INSERT INTO `sys_dict_data` VALUES ('20', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '导出操作');
INSERT INTO `sys_dict_data` VALUES ('21', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '导入操作');
INSERT INTO `sys_dict_data` VALUES ('22', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '强退操作');
INSERT INTO `sys_dict_data` VALUES ('23', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生成操作');
INSERT INTO `sys_dict_data` VALUES ('24', '8', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '清空操作');
INSERT INTO `sys_dict_data` VALUES ('25', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('26', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('27', '1', 'HTTP接口', '0', 'testmanagmt_case_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', 'HTTP接口');
INSERT INTO `sys_dict_data` VALUES ('28', '2', 'Web UI', '1', 'testmanagmt_case_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', 'Web UI');
INSERT INTO `sys_dict_data` VALUES ('29', '3', 'API驱动', '2', 'testmanagmt_case_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', 'API驱动');
INSERT INTO `sys_dict_data` VALUES ('30', '4', '移动端', '3', 'testmanagmt_case_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '移动端');
INSERT INTO `sys_dict_data` VALUES ('31', '1', '中断', '0', 'testmanagmt_case_stepfailcontinue', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '后续步骤中断');
INSERT INTO `sys_dict_data` VALUES ('32', '2', '继续', '1', 'testmanagmt_case_stepfailcontinue', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '后续步骤继续');
INSERT INTO `sys_dict_data` VALUES ('33', '1', 'String', '0', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 String');
INSERT INTO `sys_dict_data` VALUES ('34', '2', 'JSON对象', '1', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 JSON对象');
INSERT INTO `sys_dict_data` VALUES ('35', '3', 'JSONARR对象', '2', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 JSONARR对象');
INSERT INTO `sys_dict_data` VALUES ('36', '4', 'File对象', '3', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 File对象');
INSERT INTO `sys_dict_data` VALUES ('37', '5', 'Number对象', '4', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 Number对象');
INSERT INTO `sys_dict_data` VALUES ('38', '6', 'Boolean对象', '5', 'testmanagmt_templateparams_type', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型 Boolean对象');
INSERT INTO `sys_dict_data` VALUES ('39', '1', '未执行', '0', 'task_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务执行状态');
INSERT INTO `sys_dict_data` VALUES ('40', '2', '执行中', '1', 'task_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务执行状态');
INSERT INTO `sys_dict_data` VALUES ('41', '3', '执行完成', '2', 'task_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务执行状态');
INSERT INTO `sys_dict_data` VALUES ('42', '4', '执行失败', '3', 'task_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务执行状态');
INSERT INTO `sys_dict_data` VALUES ('43', '5', '唤起客户端失败', '4', 'task_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务执行状态');
INSERT INTO `sys_dict_data` VALUES ('44', '1', '成功', '0', 'case_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例执行状态');
INSERT INTO `sys_dict_data` VALUES ('45', '2', '失败', '1', 'case_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例执行状态');
INSERT INTO `sys_dict_data` VALUES ('46', '3', '锁定', '2', 'case_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例执行状态');
INSERT INTO `sys_dict_data` VALUES ('47', '4', '执行中', '3', 'case_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例执行状态');
INSERT INTO `sys_dict_data` VALUES ('48', '5', '未执行', '4', 'case_execute_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例执行状态');
INSERT INTO `sys_dict_data` VALUES ('49', '1', '已发生-初始状态', '0', 'qa_accident_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故状态');
INSERT INTO `sys_dict_data` VALUES ('50', '2', '已发生-跟踪中-未处理', '1', 'qa_accident_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故状态');
INSERT INTO `sys_dict_data` VALUES ('51', '3', '已发生-跟踪中-处理中', '2', 'qa_accident_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故状态');
INSERT INTO `sys_dict_data` VALUES ('52', '4', '跟踪处理完成', '3', 'qa_accident_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故状态');
INSERT INTO `sys_dict_data` VALUES ('53', '1', '一级事故', '1', 'qa_accident_level', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级');
INSERT INTO `sys_dict_data` VALUES ('54', '2', '二级事故', '2', 'qa_accident_level', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级');
INSERT INTO `sys_dict_data` VALUES ('55', '3', '三级事故', '3', 'qa_accident_level', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级');
INSERT INTO `sys_dict_data` VALUES ('56', '4', '四级事故', '4', 'qa_accident_level', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级');
INSERT INTO `sys_dict_data` VALUES ('57', '5', '五级及以下事故', '5', 'qa_accident_level', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级');
INSERT INTO `sys_dict_data` VALUES ('58', '1', '测试人员漏测', '1', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('59', '2', '紧急上线-未测试', '2', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('60', '3', '紧急上线-漏测', '3', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('61', '4', '开发私自上线-未测试', '4', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('62', '5', '风险分析不足', '5', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('63', '6', '项目文档不全', '6', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('64', '7', '生产数据处理', '7', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('65', '8', '需求或设计不合理', '8', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('66', '9', '无法测试', '9', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('67', '10', '系统配置异常', '10', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('68', '11', '数据库异常', '11', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('69', '12', '网络异常', '12', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('70', '13', '服务器硬件异常', '13', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('71', '14', '外部原因异常', '14', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('72', '15', '未知原因异常', '15', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('73', '16', '其他异常', '16', 'qa_accident_type', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型');
INSERT INTO `sys_dict_data` VALUES ('74', '1', '计划中', '1', 'qa_version_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本当前状态');
INSERT INTO `sys_dict_data` VALUES ('75', '2', '开发中', '2', 'qa_version_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本当前状态');
INSERT INTO `sys_dict_data` VALUES ('76', '3', '测试中', '3', 'qa_version_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本当前状态');
INSERT INTO `sys_dict_data` VALUES ('77', '4', '待上线', '4', 'qa_version_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本当前状态');
INSERT INTO `sys_dict_data` VALUES ('78', '5', '已完成', '0', 'qa_version_status', '', 'info', 'N', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本当前状态');
INSERT INTO `sys_dict_data` VALUES ('1000', '1', 'HttpClientPost发送Post请求', 'HttpClientPost', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpClient发送post请求');
INSERT INTO `sys_dict_data` VALUES ('1001', '2', 'HttpClientGet发送Get请求', 'HttpClientGet', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpClient发送get请求');
INSERT INTO `sys_dict_data` VALUES ('1002', '3', 'HttpClientPostJSON发送JSON格式Post请求', 'HttpClientPostJSON', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpClient发送JSON格式post请求');
INSERT INTO `sys_dict_data` VALUES ('1003', '4', 'httpClientPut发送Put请求', 'httpClientPut', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用httpClientPut发送put请求');
INSERT INTO `sys_dict_data` VALUES ('1004', '5', 'httpClientPutJson发送JSON格式Put请求', 'HttpClientPutJson', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用httpClientPutJson发送put请求');
INSERT INTO `sys_dict_data` VALUES ('1005', '6', 'httpClientUploadFile上传文件', 'HttpClientUploadFile', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用httpClientUploadFile上传文件');
INSERT INTO `sys_dict_data` VALUES ('1006', '7', 'HttpURLPost发送Post请求', 'HttpURLPost', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpURLConnection发送post请求');
INSERT INTO `sys_dict_data` VALUES ('1007', '8', 'URLPost发送Post请求', 'URLPost', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用URLConnection发送post');
INSERT INTO `sys_dict_data` VALUES ('1008', '9', 'GetAndSaveFile保存下载文件到客户端', 'GetAndSaveFile', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '发送get请求保存下载文件到客户端');
INSERT INTO `sys_dict_data` VALUES ('1009', '10', 'HttpURLGet发送Get请求', 'HttpURLGet', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpURLConnection发送get请求');
INSERT INTO `sys_dict_data` VALUES ('1010', '11', 'URLGet发送Get请求', 'URLGet', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用URLConnection发送get请求');
INSERT INTO `sys_dict_data` VALUES ('1011', '12', 'HttpURLDelete发送Delete请求', 'HttpURLDelete', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpURLDelete发送delete请求');
INSERT INTO `sys_dict_data` VALUES ('1012', '13', 'httpClientPostXml发送XMLPost请求', 'HttpClientPostXml', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '使用HttpClientPostXml发送XMLPost请求');
INSERT INTO `sys_dict_data` VALUES ('1013', '14', 'httpClientDeleteJson发送Delete请求', 'httpClientDeleteJson', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2020-01-15 16:20:32', 'luckyframe', '2020-01-15 16:20:32', '使用httpClientDeleteJson发送delete Json请求');
INSERT INTO `sys_dict_data` VALUES ('1014', '15', 'httpClientPatchJson发送Patch请求', 'httpClientPatchJson', 'testmanagmt_casestep_httpoperation', '', 'info', 'Y', '0', 'admin', '2020-01-15 16:20:32', 'luckyframe', '2020-01-15 16:20:32', '使用httpClientPatchJson发送PatchJson请求');
INSERT INTO `sys_dict_data` VALUES ('2000', '1', 'Click点击对象', 'click', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '点击对象');
INSERT INTO `sys_dict_data` VALUES ('2001', '2', 'Sendkeys输入', 'sendkeys', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '输入');
INSERT INTO `sys_dict_data` VALUES ('2002', '3', 'Clear清除输入框', 'clear', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '清除输入框');
INSERT INTO `sys_dict_data` VALUES ('2003', '4', 'Gotoframe跳转iframe框架', 'gotoframe', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '跳转框架（iframe）');
INSERT INTO `sys_dict_data` VALUES ('2004', '5', 'Isenabled判断对象是否可用', 'isenabled', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断对象是否可用');
INSERT INTO `sys_dict_data` VALUES ('2005', '6', 'Isdisplayed判断对象是否可见', 'isdisplayed', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断对象是否可见');
INSERT INTO `sys_dict_data` VALUES ('2006', '7', 'Exjsob针对对象执行JS脚本', 'exjsob', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '针对对象执行JS脚本');
INSERT INTO `sys_dict_data` VALUES ('2007', '8', 'Gettext获取对象文本属性', 'gettext', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象文本属性');
INSERT INTO `sys_dict_data` VALUES ('2008', '9', 'Gettagname获取对象标签类型', 'gettagname', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象标签类型');
INSERT INTO `sys_dict_data` VALUES ('2009', '10', 'Getcaptcha获取对象中的验证码(识别率较低)', 'getcaptcha', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象中的验证码(识别率较低)');
INSERT INTO `sys_dict_data` VALUES ('2010', '11', 'Selectbyvisibletext通过下拉框的文本进行选择', 'selectbyvisibletext', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的文本进行选择');
INSERT INTO `sys_dict_data` VALUES ('2011', '12', 'Selectbyvalue通过下拉框的VALUE属性进行选择', 'selectbyvalue', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的VALUE属性进行选择');
INSERT INTO `sys_dict_data` VALUES ('2012', '13', 'Selectbyindex通过下拉框的index属性进行选择(从0开始)', 'selectbyindex', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的index属性进行选择(从0开始)');
INSERT INTO `sys_dict_data` VALUES ('2013', '14', 'Isselect判断是否已经被选择，同用于单选复选框', 'isselect', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断是否已经被选择，同用于单选复选框');
INSERT INTO `sys_dict_data` VALUES ('2014', '15', 'Open打开URL', 'open', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '打开URL');
INSERT INTO `sys_dict_data` VALUES ('2015', '16', 'Exjs执行js脚本', 'exjs', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '执行js脚本');
INSERT INTO `sys_dict_data` VALUES ('2016', '17', 'Gotodefaultcontent跳转回到默认iframe', 'gotodefaultcontent', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '跳转回到默认iframe');
INSERT INTO `sys_dict_data` VALUES ('2017', '18', 'Gettitle获取窗口标题', 'gettitle', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取窗口标题');
INSERT INTO `sys_dict_data` VALUES ('2018', '19', 'Getwindowhandle获取窗口句柄', 'getwindowhandle', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取窗口句柄');
INSERT INTO `sys_dict_data` VALUES ('2019', '20', 'Gotowindow跳转窗口句柄', 'gotowindow', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '跳转窗口句柄');
INSERT INTO `sys_dict_data` VALUES ('2020', '21', 'Timeout设置全局隐式等待时间(S)', 'timeout', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '设置全局隐式等待时间(S)');
INSERT INTO `sys_dict_data` VALUES ('2021', '22', 'Alertaccept弹出框点击OK', 'alertaccept', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '弹出框点击OK');
INSERT INTO `sys_dict_data` VALUES ('2022', '23', 'Alertdismiss弹出框点击取消', 'alertdismiss', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '弹出框点击取消');
INSERT INTO `sys_dict_data` VALUES ('2023', '24', 'Alertgettext获取弹出框TEXT', 'alertgettext', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取弹出框TEXT');
INSERT INTO `sys_dict_data` VALUES ('2024', '25', 'Mouselkclick模拟鼠标左键单击(可带页面对象)', 'mouselkclick', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标左键单击(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2025', '26', 'Mouserkclick模拟鼠标右键单击(可带页面对象)', 'mouserkclick', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标右键单击(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2026', '27', 'Mousedclick模拟鼠标双击(可带页面对象)', 'mousedclick', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标双击(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2027', '28', 'Mouseclickhold模拟鼠标左键单击后不释放(可带页面对象)', 'mouseclickhold', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标左键单击后不释放(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2028', '29', 'Mousedrag模拟鼠标拖拽(可带页面对象)', 'mousedrag', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标拖拽(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2029', '30', 'Mouseto模拟鼠标移动到指定坐标(可带页面对象)', 'mouseto', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标移动到指定坐标(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2030', '31', 'Mouserelease模拟鼠标释放(可带页面对象)', 'mouserelease', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟鼠标释放(可带页面对象)');
INSERT INTO `sys_dict_data` VALUES ('2031', '32', 'Mousekey(tab)模拟键盘Tab键', 'mousekey(tab)', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟键盘Tab键');
INSERT INTO `sys_dict_data` VALUES ('2032', '33', 'Mousekey(space)模拟键盘Space键', 'mousekey(space)', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟键盘Space键');
INSERT INTO `sys_dict_data` VALUES ('2033', '34', 'Mousekey(ctrl)模拟键盘Ctrl键', 'mousekey(ctrl)', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟键盘Ctrl键');
INSERT INTO `sys_dict_data` VALUES ('2034', '35', 'Mousekey(shift)模拟键盘Shift键', 'mousekey(shift)', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟键盘Shift键');
INSERT INTO `sys_dict_data` VALUES ('2035', '36', 'Mousekey(enter)模拟键盘Enter键', 'mousekey(enter)', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模拟键盘Enter键');
INSERT INTO `sys_dict_data` VALUES ('2036', '37', 'Runcase调用指定接口|Web UI用例', 'runcase', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '调用指定接口|Web UI用例');
INSERT INTO `sys_dict_data` VALUES ('2037', '38', 'Getattribute获取对象指定属性', 'getattribute', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象指定属性');
INSERT INTO `sys_dict_data` VALUES ('2038', '39', 'Getcssvalue获取对象指定CSS属性值', 'getcssvalue', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象指定CSS属性值');
INSERT INTO `sys_dict_data` VALUES ('2039', '40', 'Gotoparentframe跳转回到上一级iframe', 'gotoparentframe', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '跳转回到上一级iframe');
INSERT INTO `sys_dict_data` VALUES ('2040', '41', 'Scrollto滚动到目标对象', 'scrollto', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '滚动到目标对象');
INSERT INTO `sys_dict_data` VALUES ('2041', '42', 'Scrollintoview将目标对象滚动到可视', 'scrollintoview', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '将目标对象滚动到可视');
INSERT INTO `sys_dict_data` VALUES ('2042', '43', 'Closewindow关闭当前浏览器窗口', 'closewindow', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '关闭当前浏览器窗口');
INSERT INTO `sys_dict_data` VALUES ('2043', '44', 'Addcookie添加浏览器cookie', 'addcookie', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '添加浏览器cookie');
INSERT INTO `sys_dict_data` VALUES ('2044', '45', 'PageRefresh刷新当前页面', 'pagerefresh', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '刷新当前页面');
INSERT INTO `sys_dict_data` VALUES ('2045', '46', 'PageForward前进当前页面', 'pageforward', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '前进当前页面');
INSERT INTO `sys_dict_data` VALUES ('2046', '47', 'PageBack回退当前页面', 'pageback', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '回退当前页面');
INSERT INTO `sys_dict_data` VALUES ('2047', '48', 'isElementExist判断元素是否存在', 'iselementexist', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断元素是否存在');
INSERT INTO `sys_dict_data` VALUES ('2048', '49', 'GetValue获取指定对象值', 'getvalue', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2021-06-30 14:27:32', 'luckyframe', '2021-06-30 14:27:32', '获取指定对象值');
INSERT INTO `sys_dict_data` VALUES ('2049', '50', 'Switchtowindow切换句柄并关闭之前窗口', 'switchtowindow', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2021-12-07 20:24:26', 'admin', '2021-12-07 20:25:39', '切换句柄并关闭之前窗口');
INSERT INTO `sys_dict_data` VALUES ('2050', '51', 'Windowsetsize设置浏览器窗口大小', 'windowsetsize', 'testmanagmt_casestep_uioperation', '', 'info', 'Y', '0', 'admin', '2021-12-30 15:26:27', 'admin', '2021-12-30 15:26:43', '设置浏览器窗口大小');
INSERT INTO `sys_dict_data` VALUES ('3000', '1', 'Selectbyvisibletext通过下拉框的文本进行选择', 'selectbyvisibletext', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的文本进行选择');
INSERT INTO `sys_dict_data` VALUES ('3001', '2', 'Selectbyvalue通过下拉框的VALUE属性进行选择', 'selectbyvalue', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的VALUE属性进行选择');
INSERT INTO `sys_dict_data` VALUES ('3002', '3', 'Selectbyindex通过下拉框的index属性进行选择(从0开始)', 'selectbyindex', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通过下拉框的index属性进行选择(从0开始)');
INSERT INTO `sys_dict_data` VALUES ('3003', '4', 'Isselect判断是否已经被选择，同用于单选复选框', 'isselect', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断是否已经被选择，同用于单选复选框');
INSERT INTO `sys_dict_data` VALUES ('3004', '5', 'Gettext获取对象文本属性', 'gettext', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象文本属性');
INSERT INTO `sys_dict_data` VALUES ('3005', '6', 'Gettagname获取对象标签类型', 'gettagname', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象标签类型');
INSERT INTO `sys_dict_data` VALUES ('3006', '7', 'Getattribute获取对象指定属性', 'getattribute', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象指定属性');
INSERT INTO `sys_dict_data` VALUES ('3007', '8', 'Getcssvalue获取对象指定CSS属性值', 'getcssvalue', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取对象指定CSS属性值');
INSERT INTO `sys_dict_data` VALUES ('3008', '9', 'Click点击对象', 'click', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '点击对象');
INSERT INTO `sys_dict_data` VALUES ('3009', '10', 'Sendkeys输入', 'sendkeys', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '输入');
INSERT INTO `sys_dict_data` VALUES ('3010', '11', 'Clear清除输入框', 'clear', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '清除输入框');
INSERT INTO `sys_dict_data` VALUES ('3011', '12', 'Isenabled判断对象是否可用', 'isenabled', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断对象是否可用');
INSERT INTO `sys_dict_data` VALUES ('3012', '13', 'Isdisplayed判断对象是否可见', 'isdisplayed', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '判断对象是否可见');
INSERT INTO `sys_dict_data` VALUES ('3013', '14', 'Exjsob针对对象执行JS脚本', 'exjsob', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '针对对象执行JS脚本');
INSERT INTO `sys_dict_data` VALUES ('3014', '15', 'Longpresselement长按指定页面对象(可设置时间)', 'longpresselement', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '长按指定页面对象(可设置时间)');
INSERT INTO `sys_dict_data` VALUES ('3015', '16', 'Alertaccept弹出框点击OK', 'alertaccept', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '弹出框点击OK');
INSERT INTO `sys_dict_data` VALUES ('3016', '17', 'Alertdismiss弹出框点击取消', 'alertdismiss', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '弹出框点击取消');
INSERT INTO `sys_dict_data` VALUES ('3017', '18', 'Alertgettext获取弹出框TEXT', 'alertgettext', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取弹出框TEXT');
INSERT INTO `sys_dict_data` VALUES ('3018', '19', 'Getcontexthandles获取指定context的值(参数指定1 第一个)', 'getcontexthandles', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取指定context的值(参数指定1 第一个)');
INSERT INTO `sys_dict_data` VALUES ('3019', '20', 'Exjs执行JS脚本', 'exjs', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '执行JS脚本');
INSERT INTO `sys_dict_data` VALUES ('3020', '21', 'AndroidKeyCode安卓模拟手机按键，参考Android KeyCode', 'androidkeycode', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', 'AndroidKeyCode安卓模拟手机按键，参考Android KeyCode');
INSERT INTO `sys_dict_data` VALUES ('3021', '22', 'Gotocontext跳转到指定的context', 'gotocontext', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '跳转到指定的context');
INSERT INTO `sys_dict_data` VALUES ('3022', '23', 'Getcontext获取当前窗口的context', 'getcontext', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取当前窗口的context');
INSERT INTO `sys_dict_data` VALUES ('3023', '24', 'Gettitle获取当前窗口的title', 'gettitle', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '获取当前窗口的title');
INSERT INTO `sys_dict_data` VALUES ('3024', '25', 'SwipePageUp页面向上滑动(参数 持续时间|滚动次数)', 'swipepageup', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '页面向上滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3025', '26', 'SwipePageDown页面向下滑动(参数 持续时间|滚动次数)', 'swipepagedown', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '页面向下滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3026', '27', 'SwipePageLeft页面向左滑动(参数 持续时间|滚动次数)', 'swipepageleft', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '页面向左滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3027', '28', 'SwipePageRight页面向右滑动(参数 持续时间|滚动次数)', 'swipepageright', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '页面向右滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3028', '29', 'Longpressxy长按指定坐标(参数 X坐标|Y坐标|持续时间(可选))', 'longpressxy', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '长按指定坐标(参数 X坐标|Y坐标|持续时间(可选))');
INSERT INTO `sys_dict_data` VALUES ('3029', '30', 'Pressxy点击指定坐标(参数 X坐标|Y坐标)', 'pressxy', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '点击指定坐标(参数 X坐标|Y坐标)');
INSERT INTO `sys_dict_data` VALUES ('3030', '31', 'Tapxy轻击指定坐标(参数 X坐标|Y坐标)', 'tapxy', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '轻击指定坐标(参数 X坐标|Y坐标)');
INSERT INTO `sys_dict_data` VALUES ('3031', '32', 'JspressxyS方式点击指定坐标(参数 X坐标|Y坐标|持续时间(可选))', 'jspressxy', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', 'JS方式点击指定坐标(参数 X坐标|Y坐标|持续时间(可选))');
INSERT INTO `sys_dict_data` VALUES ('3032', '33', 'Moveto拖动坐标(参数 startX,startY|X,Y|X,Y|X,Y...)', 'moveto', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '拖动坐标(参数 startX,startY|X,Y|X,Y|X,Y...)');
INSERT INTO `sys_dict_data` VALUES ('3033', '34', 'Screenshot保存当前页面截图', 'screenshot', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '保存当前页面截图');
INSERT INTO `sys_dict_data` VALUES ('3034', '35', 'Timeout设置全局页面加载&元素出现最大等待时间(S)', 'timeout', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '设置全局页面加载&元素出现最大等待时间(S)');
INSERT INTO `sys_dict_data` VALUES ('3035', '36', 'HideKeyboard隐藏系统手机键盘', 'hideKeyboard', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '隐藏系统手机键盘');
INSERT INTO `sys_dict_data` VALUES ('3036', '37', 'Runcase调用指定接口用例', 'runcase', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '调用指定接口用例');
INSERT INTO `sys_dict_data` VALUES ('3037', '38', 'ExAdbShell执行安卓adb命令', 'exAdbShell', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '执行安卓adb命令');
INSERT INTO `sys_dict_data` VALUES ('3038', '39', 'SwipeUp手指向上滑动(参数 持续时间|滚动次数)', 'swipeup', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2020-01-19 09:27:32', 'luckyframe', '2020-01-19 09:27:32', '手指向上滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3039', '40', 'SwipeDown手指向下滑动(参数 持续时间|滚动次数)', 'swipedown', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2020-01-19 09:27:32', 'luckyframe', '2020-01-19 09:27:32', '手指向下滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3040', '41', 'SwipeLeft手指向左滑动(参数 持续时间|滚动次数)', 'swipeleft', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2020-01-19 09:27:32', 'luckyframe', '2020-01-19 09:27:32', '手指向左滑动(参数 持续时间|滚动次数)');
INSERT INTO `sys_dict_data` VALUES ('3041', '42', 'SwipeRight手指向右滑动(参数 持续时间|滚动次数)', 'swiperight', 'testmanagmt_casestep_muioperation', '', 'info', 'Y', '0', 'admin', '2020-01-19 09:27:32', 'luckyframe', '2020-01-19 09:27:32', '手指向右滑动(参数 持续时间|滚动次数)');

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '系统是否', 'sys_yes_no', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('6', '通知类型', 'sys_notice_type', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知状态', 'sys_notice_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('8', '操作类型', 'sys_oper_type', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('9', '系统状态', 'sys_common_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('10', '用例类型', 'testmanagmt_case_type', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例步骤类型');
INSERT INTO `sys_dict_type` VALUES ('11', '步骤失败策略', 'testmanagmt_case_stepfailcontinue', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '步骤失败策略');
INSERT INTO `sys_dict_type` VALUES ('12', '模板参数类型', 'testmanagmt_templateparams_type', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '模板参数类型');
INSERT INTO `sys_dict_type` VALUES ('13', '步骤HTTP操作类型', 'testmanagmt_casestep_httpoperation', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '步骤关键字操作字典');
INSERT INTO `sys_dict_type` VALUES ('14', '步骤Web UI操作类型', 'testmanagmt_casestep_uioperation', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '步骤关键字操作字典');
INSERT INTO `sys_dict_type` VALUES ('15', '步骤移动端操作类型', 'testmanagmt_casestep_muioperation', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '步骤关键字操作字典');
INSERT INTO `sys_dict_type` VALUES ('16', '测试任务执行状态', 'task_execute_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试任务执行状态字典');
INSERT INTO `sys_dict_type` VALUES ('17', '测试用例执行状态', 'case_execute_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试用例执行状态字典');
INSERT INTO `sys_dict_type` VALUES ('18', '生产事故登记状态', 'qa_accident_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故状态字典');
INSERT INTO `sys_dict_type` VALUES ('19', '生产事故等级', 'qa_accident_level', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故等级字典');
INSERT INTO `sys_dict_type` VALUES ('20', '生产事故类型', 'qa_accident_type', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故类型字典');
INSERT INTO `sys_dict_type` VALUES ('21', '项目版本状态', 'qa_version_status', '0', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目版本状态字典');

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '调度任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '调度任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT '' COMMENT '调度任务组名',
  `method_name` varchar(500) DEFAULT '' COMMENT '调度任务方法',
  `method_params` varchar(50) DEFAULT NULL COMMENT '方法参数',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行策略（1立即执行 2执行一次 3放弃执行）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('100', 'clientHeart', '客户端心跳监听', 'heartTask', '127.0.0.1', '0/30 * * * * ? ', '3', '0', 'admin', '2022-05-13 18:25:45', 'admin', '2022-05-15 11:22:54', '');

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `method_name` varchar(500) DEFAULT NULL COMMENT '任务方法',
  `method_params` varchar(50) DEFAULT NULL COMMENT '方法参数',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('100', '13311273513', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-05-13 17:35:20');
INSERT INTO `sys_logininfor` VALUES ('101', '13311273513', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-05-13 17:35:24');
INSERT INTO `sys_logininfor` VALUES ('102', '13311273513', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-05-13 17:35:58');
INSERT INTO `sys_logininfor` VALUES ('103', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-05-13 17:36:46');
INSERT INTO `sys_logininfor` VALUES ('104', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2022-05-15 14:57:35');
INSERT INTO `sys_logininfor` VALUES ('105', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2022-05-15 14:57:38');
INSERT INTO `sys_logininfor` VALUES ('106', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2022-05-15 14:57:43');
INSERT INTO `sys_logininfor` VALUES ('107', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2022-05-15 14:57:51');
INSERT INTO `sys_logininfor` VALUES ('108', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-05-15 14:57:59');
INSERT INTO `sys_logininfor` VALUES ('109', 'admin', '124.127.150.19', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-19 13:23:58');
INSERT INTO `sys_logininfor` VALUES ('110', 'test', '124.127.150.19', 'XX XX', 'Safari 9', 'Mac OS X', '1', '用户不存在/密码错误', '2022-06-19 13:24:29');
INSERT INTO `sys_logininfor` VALUES ('111', 'admin', '111.193.0.112', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2022-06-19 13:26:47');
INSERT INTO `sys_logininfor` VALUES ('112', 'admin', '124.127.150.19', 'XX XX', 'Safari 9', 'Mac OS X', '0', '退出成功', '2022-06-19 14:00:34');
INSERT INTO `sys_logininfor` VALUES ('113', 'test', '124.127.150.19', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-19 14:00:38');
INSERT INTO `sys_logininfor` VALUES ('114', 'test', '124.127.150.19', 'XX XX', 'Safari 9', 'Mac OS X', '0', '退出成功', '2022-06-19 14:00:51');
INSERT INTO `sys_logininfor` VALUES ('115', 'admin', '111.193.0.112', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-19 14:10:56');
INSERT INTO `sys_logininfor` VALUES ('116', 'test', '111.193.0.112', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-19 14:23:15');
INSERT INTO `sys_logininfor` VALUES ('117', 'admin', '111.193.0.112', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-19 14:24:27');
INSERT INTO `sys_logininfor` VALUES ('118', 'admin', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '退出成功', '2022-06-20 02:07:13');
INSERT INTO `sys_logininfor` VALUES ('119', 'test', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-20 02:07:15');
INSERT INTO `sys_logininfor` VALUES ('120', 'admin', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-20 02:07:34');
INSERT INTO `sys_logininfor` VALUES ('121', 'admin', '123.117.116.121', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-20 02:25:17');
INSERT INTO `sys_logininfor` VALUES ('122', 'admin', '123.117.116.121', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-20 02:29:06');
INSERT INTO `sys_logininfor` VALUES ('123', 'admin', '123.117.116.121', 'XX XX', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-06-20 02:33:00');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '退出成功', '2022-06-20 02:45:52');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-20 02:45:57');
INSERT INTO `sys_logininfor` VALUES ('126', 'test', '123.117.116.121', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '1', '对不起，您的账号已被删除', '2022-06-20 03:12:51');
INSERT INTO `sys_logininfor` VALUES ('127', 'test', '123.117.116.121', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '1', '对不起，您的账号已被删除', '2022-06-20 03:12:52');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-06-20 03:20:16');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '123.117.116.121', 'XX XX', 'Safari 9', 'Mac OS X', '0', '登录成功', '2022-06-20 04:04:48');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '123.117.116.121', 'XX XX', 'Apple WebKit', 'Mac OS X (iPhone)', '0', '登录成功', '2022-06-20 04:07:08');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-06-20 06:01:04');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误2次', '2022-06-20 06:02:28');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误3次', '2022-06-20 06:16:00');
INSERT INTO `sys_logininfor` VALUES ('134', 'test', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '1', '对不起，您的账号已被删除', '2022-06-20 06:16:42');
INSERT INTO `sys_logininfor` VALUES ('135', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-06-20 06:19:10');
INSERT INTO `sys_logininfor` VALUES ('136', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误2次', '2022-06-20 06:19:32');
INSERT INTO `sys_logininfor` VALUES ('137', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误3次', '2022-06-20 06:19:43');
INSERT INTO `sys_logininfor` VALUES ('138', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误4次', '2022-06-20 06:19:54');
INSERT INTO `sys_logininfor` VALUES ('139', 'admin', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-06-20 07:50:47');
INSERT INTO `sys_logininfor` VALUES ('140', 'admin', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误2次', '2022-06-20 07:52:19');
INSERT INTO `sys_logininfor` VALUES ('141', 'admin', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误3次', '2022-06-20 07:52:54');
INSERT INTO `sys_logininfor` VALUES ('142', 'admin', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误4次', '2022-06-20 07:54:19');
INSERT INTO `sys_logininfor` VALUES ('143', 'admin', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-06-20 08:49:35');
INSERT INTO `sys_logininfor` VALUES ('144', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误1次', '2022-06-20 09:51:59');
INSERT INTO `sys_logininfor` VALUES ('145', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误2次', '2022-06-20 09:55:42');
INSERT INTO `sys_logininfor` VALUES ('146', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误3次', '2022-06-20 09:56:45');
INSERT INTO `sys_logininfor` VALUES ('147', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误4次', '2022-06-20 11:13:33');
INSERT INTO `sys_logininfor` VALUES ('148', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:21:02');
INSERT INTO `sys_logininfor` VALUES ('149', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '密码输入错误5次', '2022-06-20 11:26:33');
INSERT INTO `sys_logininfor` VALUES ('150', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:28:27');
INSERT INTO `sys_logininfor` VALUES ('151', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:28:58');
INSERT INTO `sys_logininfor` VALUES ('152', 'admin', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '1', '密码输入错误5次，帐户锁定10分钟', '2022-06-20 11:29:44');
INSERT INTO `sys_logininfor` VALUES ('153', 'admin', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '1', '密码输入错误5次，帐户锁定10分钟', '2022-06-20 11:30:01');
INSERT INTO `sys_logininfor` VALUES ('154', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:34:28');
INSERT INTO `sys_logininfor` VALUES ('155', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:34:30');
INSERT INTO `sys_logininfor` VALUES ('156', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:35:58');
INSERT INTO `sys_logininfor` VALUES ('157', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:39:34');
INSERT INTO `sys_logininfor` VALUES ('158', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:39:37');
INSERT INTO `sys_logininfor` VALUES ('159', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:40:32');
INSERT INTO `sys_logininfor` VALUES ('160', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:41:50');
INSERT INTO `sys_logininfor` VALUES ('161', '', '192.168.1.10', '内网IP', 'Chrome 10', 'Windows 10', '1', '* 必须填写', '2022-06-20 11:41:55');
INSERT INTO `sys_logininfor` VALUES ('162', 'admin', '123.117.116.121', 'XX XX', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-06-22 01:21:56');
INSERT INTO `sys_logininfor` VALUES ('163', 'admin', '192.168.1.9', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-06-22 14:31:58');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1115 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '4', '#', 'M', '0', '', 'fa fa-gear', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '5', '#', 'M', '0', '', 'fa fa-video-camera', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '6', '#', 'M', '0', '', 'fa fa-bars', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('4', '测试管理', '0', '1', '#', 'M', '0', '', 'fa fa-th-large', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试过程管理');
INSERT INTO `sys_menu` VALUES ('5', '测试执行', '0', '2', '#', 'M', '0', '', 'fa fa-rocket', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试执行管理');
INSERT INTO `sys_menu` VALUES ('6', '质量管理', '0', '3', '#', 'M', '0', '', 'fa fa-bug', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '质量过程管理');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', '/system/user', 'C', '0', 'system:user:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', '/system/role', 'C', '0', 'system:role:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '3', '5', '/system/menu', 'C', '0', 'system:menu:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', '/system/dept', 'C', '0', 'system:dept:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '项目管理', '1', '5', '/system/project', 'C', '0', 'system:project:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '项目管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '客户端管理', '1', '6', '/system/client', 'C', '0', 'system:client:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '客户端管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '岗位管理', '1', '8', '/system/post', 'C', '0', 'system:post:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('107', '字典管理', '3', '4', '/system/dict', 'C', '0', 'system:dict:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('108', '参数设置', '1', '9', '/system/config', 'C', '0', 'system:config:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('109', '通知公告', '1', '10', '/system/notice', 'C', '0', 'system:notice:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('110', '日志管理', '1', '11', '#', 'M', '0', '', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('111', '在线用户', '2', '1', '/monitor/online', 'C', '0', 'monitor:online:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('112', '定时任务', '2', '2', '/monitor/job', 'C', '0', 'monitor:job:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('113', '数据监控', '2', '3', '/monitor/data', 'C', '0', 'monitor:data:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '服务监控', '2', '4', '/monitor/server', 'C', '0', 'monitor:server:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('115', '表单构建', '3', '1', '/tool/build', 'C', '0', 'tool:build:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('116', '代码生成', '3', '2', '/tool/gen', 'C', '0', 'tool:gen:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('117', '系统接口', '3', '3', '/tool/swagger', 'C', '0', 'tool:swagger:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('118', '用例管理', '4', '1', '/testmanagmt/projectCase', 'C', '0', 'testmanagmt:projectCase:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2018-03-01 00:00:00', '测试用例管理菜单');
INSERT INTO `sys_menu` VALUES ('119', '用例模块', '4', '2', '/testmanagmt/projectCaseModule', 'C', '0', 'testmanagmt:projectCaseModule:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2018-03-01 00:00:00', '测试用例模块管理菜单');
INSERT INTO `sys_menu` VALUES ('120', '协议模板', '4', '3', '/testmanagmt/projectProtocolTemplate', 'C', '0', 'testmanagmt:projectProtocolTemplate:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '协议模板管理菜单');
INSERT INTO `sys_menu` VALUES ('121', '测试计划', '4', '4', '/testmanagmt/projectPlan', 'C', '0', 'testmanagmt:projectPlan:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试计划菜单');
INSERT INTO `sys_menu` VALUES ('122', '公共参数', '4', '5', '/testmanagmt/projectCaseParams', 'C', '0', 'testmanagmt:projectCaseParams:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例公共参数菜单');
INSERT INTO `sys_menu` VALUES ('123', '任务调度', '5', '1', '/testexecution/taskScheduling', 'C', '0', 'testexecution:taskScheduling:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试任务调度菜单');
INSERT INTO `sys_menu` VALUES ('124', '任务执行', '5', '2', '/testexecution/taskExecute', 'C', '0', 'testexecution:taskExecute:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '测试任务执行菜单');
INSERT INTO `sys_menu` VALUES ('125', '用例明细', '5', '3', '/testexecution/taskCaseExecute', 'C', '0', 'testexecution:taskCaseExecute:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '用例明细菜单');
INSERT INTO `sys_menu` VALUES ('126', '生产事故', '6', '2', '/qualitymanagmt/qaAccident', 'C', '0', 'qualitymanagmt:qaAccident:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '生产事故菜单');
INSERT INTO `sys_menu` VALUES ('127', '版本管理', '6', '1', '/qualitymanagmt/qaVersion', 'C', '0', 'qualitymanagmt:qaVersion:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '版本管理菜单');
INSERT INTO `sys_menu` VALUES ('128', '客户端配置', '1', '7', '/system/clientConfig', 'C', '0', 'system:clientConfig:view', '#', 'admin', '2020-07-07 19:15:32', 'luckyframe', '2020-07-07 19:15:32', '客户端配置菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '110', '1', '/monitor/operlog', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '110', '2', '/monitor/logininfor', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '#', 'F', '0', 'system:user:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '#', 'F', '0', 'system:user:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '#', 'F', '0', 'system:user:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '#', 'F', '0', 'system:user:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '#', 'F', '0', 'system:user:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', '6', '#', 'F', '0', 'system:user:import', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', '7', '#', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', '1', '#', 'F', '0', 'system:role:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', '2', '#', 'F', '0', 'system:role:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', '3', '#', 'F', '0', 'system:role:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', '4', '#', 'F', '0', 'system:role:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', '5', '#', 'F', '0', 'system:role:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单查询', '102', '1', '#', 'F', '0', 'system:menu:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单新增', '102', '2', '#', 'F', '0', 'system:menu:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单修改', '102', '3', '#', 'F', '0', 'system:menu:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单删除', '102', '4', '#', 'F', '0', 'system:menu:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', '1', '#', 'F', '0', 'system:dept:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', '2', '#', 'F', '0', 'system:dept:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', '3', '#', 'F', '0', 'system:dept:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', '4', '#', 'F', '0', 'system:dept:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '106', '1', '#', 'F', '0', 'system:post:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '106', '2', '#', 'F', '0', 'system:post:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '106', '3', '#', 'F', '0', 'system:post:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '106', '4', '#', 'F', '0', 'system:post:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '106', '5', '#', 'F', '0', 'system:post:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1025', '字典查询', '107', '1', '#', 'F', '0', 'system:dict:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1026', '字典新增', '107', '2', '#', 'F', '0', 'system:dict:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1027', '字典修改', '107', '3', '#', 'F', '0', 'system:dict:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1028', '字典删除', '107', '4', '#', 'F', '0', 'system:dict:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1029', '字典导出', '107', '5', '#', 'F', '0', 'system:dict:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1030', '参数查询', '108', '1', '#', 'F', '0', 'system:config:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1031', '参数新增', '108', '2', '#', 'F', '0', 'system:config:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1032', '参数修改', '108', '3', '#', 'F', '0', 'system:config:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1033', '参数删除', '108', '4', '#', 'F', '0', 'system:config:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1034', '参数导出', '108', '5', '#', 'F', '0', 'system:config:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '109', '1', '#', 'F', '0', 'system:notice:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '109', '2', '#', 'F', '0', 'system:notice:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '109', '3', '#', 'F', '0', 'system:notice:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '109', '4', '#', 'F', '0', 'system:notice:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', '1', '#', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', '2', '#', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1041', '详细信息', '500', '3', '#', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1046', '在线查询', '111', '1', '#', 'F', '0', 'monitor:online:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1047', '批量强退', '111', '2', '#', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1048', '单条强退', '111', '3', '#', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1049', '任务查询', '112', '1', '#', 'F', '0', 'monitor:job:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1050', '任务新增', '112', '2', '#', 'F', '0', 'monitor:job:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1051', '任务修改', '112', '3', '#', 'F', '0', 'monitor:job:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1052', '任务删除', '112', '4', '#', 'F', '0', 'monitor:job:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1053', '状态修改', '112', '5', '#', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1054', '任务详细', '112', '6', '#', 'F', '0', 'monitor:job:detail', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1055', '任务导出', '112', '7', '#', 'F', '0', 'monitor:job:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1056', '生成查询', '116', '1', '#', 'F', '0', 'tool:gen:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1057', '生成代码', '116', '2', '#', 'F', '0', 'tool:gen:code', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1058', '项目查询', '104', '1', '#', 'F', '0', 'system:project:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1059', '项目新增', '104', '2', '#', 'F', '0', 'system:project:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1060', '项目修改', '104', '3', '#', 'F', '0', 'system:project:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1061', '项目删除', '104', '4', '#', 'F', '0', 'system:project:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1062', '客户端查询', '105', '1', '#', 'F', '0', 'system:client:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1063', '客户端新增', '105', '2', '#', 'F', '0', 'system:client:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1064', '客户端修改', '105', '3', '#', 'F', '0', 'system:client:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1065', '客户端删除', '105', '4', '#', 'F', '0', 'system:client:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1066', '用例查询', '118', '1', '#', 'F', '0', 'testmanagmt:projectCase:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1067', '用例新增', '118', '2', '#', 'F', '0', 'testmanagmt:projectCase:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1068', '用例修改', '118', '3', '#', 'F', '0', 'testmanagmt:projectCase:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1069', '用例删除', '118', '4', '#', 'F', '0', 'testmanagmt:projectCase:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1070', '模块查询', '119', '1', '#', 'F', '0', 'testmanagmt:projectCaseModule:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1071', '模块新增', '119', '2', '#', 'F', '0', 'testmanagmt:projectCaseModule:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1072', '模块修改', '119', '3', '#', 'F', '0', 'testmanagmt:projectCaseModule:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1073', '模块删除', '119', '4', '#', 'F', '0', 'testmanagmt:projectCaseModule:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1074', '协议查询', '120', '1', '#', 'F', '0', 'testmanagmt:projectProtocolTemplate:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1075', '协议新增', '120', '2', '#', 'F', '0', 'testmanagmt:projectProtocolTemplate:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1076', '协议修改', '120', '3', '#', 'F', '0', 'testmanagmt:projectProtocolTemplate:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1077', '协议删除', '120', '4', '#', 'F', '0', 'testmanagmt:projectProtocolTemplate:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1078', '计划查询', '121', '1', '#', 'F', '0', 'testmanagmt:projectPlan:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1079', '计划新增', '121', '2', '#', 'F', '0', 'testmanagmt:projectPlan:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1080', '计划修改', '121', '3', '#', 'F', '0', 'testmanagmt:projectPlan:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1081', '计划删除', '121', '4', '#', 'F', '0', 'testmanagmt:projectPlan:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1082', '参数查询', '122', '1', '#', 'F', '0', 'testmanagmt:projectCaseParams:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1083', '参数新增', '122', '2', '#', 'F', '0', 'testmanagmt:projectCaseParams:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1084', '参数修改', '122', '3', '#', 'F', '0', 'testmanagmt:projectCaseParams:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1085', '参数删除', '122', '4', '#', 'F', '0', 'testmanagmt:projectCaseParams:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1086', '调度查询', '123', '1', '#', 'F', '0', 'testexecution:taskScheduling:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1087', '调度新增', '123', '2', '#', 'F', '0', 'testexecution:taskScheduling:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1088', '调度修改', '123', '3', '#', 'F', '0', 'testexecution:taskScheduling:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1089', '调度删除', '123', '4', '#', 'F', '0', 'testexecution:taskScheduling:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1090', '调度执行', '123', '5', '#', 'F', '0', 'testexecution:taskScheduling:execution', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1091', '执行查询', '124', '1', '#', 'F', '0', 'testexecution:taskExecute:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1092', '执行删除', '124', '2', '#', 'F', '0', 'testexecution:taskExecute:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1093', '用例明细查询', '125', '1', '#', 'F', '0', 'testexecution:taskCaseExecute:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1094', '用例明细删除', '125', '2', '#', 'F', '0', 'testexecution:taskCaseExecute:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1095', '用例明细执行', '125', '3', '#', 'F', '0', 'testexecution:taskCaseExecute:execution', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1096', '用例导出', '118', '5', '#', 'F', '0', 'testmanagmt:projectCase:export', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1097', '生产事故查询', '126', '1', '#', 'F', '0', 'qualitymanagmt:qaAccident:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1098', '生产事故新增', '126', '2', '#', 'F', '0', 'qualitymanagmt:qaAccident:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1099', '生产事故修改', '126', '3', '#', 'F', '0', 'qualitymanagmt:qaAccident:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1100', '生产事故删除', '126', '4', '#', 'F', '0', 'qualitymanagmt:qaAccident:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1101', '版本管理查询', '127', '1', '#', 'F', '0', 'qualitymanagmt:qaVersion:list', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1102', '版本管理新增', '127', '2', '#', 'F', '0', 'qualitymanagmt:qaVersion:add', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1103', '版本管理修改', '127', '3', '#', 'F', '0', 'qualitymanagmt:qaVersion:edit', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1104', '版本管理删除', '127', '4', '#', 'F', '0', 'qualitymanagmt:qaVersion:remove', '#', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2019-02-13 10:27:32', '');
INSERT INTO `sys_menu` VALUES ('1105', '客户端配置查询', '128', '1', '#', 'F', '0', 'system:clientConfig:list', '#', 'admin', '2020-07-07 19:15:32', 'luckyframe', '2020-07-07 19:15:32', '');
INSERT INTO `sys_menu` VALUES ('1106', '客户端配置新增', '128', '2', '#', 'F', '0', 'system:clientConfig:add', '#', 'admin', '2020-07-07 19:15:32', 'luckyframe', '2020-07-07 19:15:32', '');
INSERT INTO `sys_menu` VALUES ('1107', '客户端配置修改', '128', '3', '#', 'F', '0', 'system:clientConfig:edit', '#', 'admin', '2020-07-07 19:15:32', 'luckyframe', '2020-07-07 19:15:32', '');
INSERT INTO `sys_menu` VALUES ('1108', '客户端配置删除', '128', '4', '#', 'F', '0', 'system:clientConfig:remove', '#', 'admin', '2020-07-07 19:15:32', 'luckyframe', '2020-07-07 19:15:32', '');
INSERT INTO `sys_menu` VALUES ('1109', '用例步骤导入', '118', '6', '#', 'F', '0', 'testmanagmt:projectCase:import', '#', 'admin', '2021-12-13 17:36:51', 'admin', '2021-12-13 17:39:59', '');
INSERT INTO `sys_menu` VALUES ('1110', '聚合计划', '4', '6', '/testmanagmt/projectSuite', 'C', '0', 'testmanagmt:projectSuite:view', '#', 'admin', '2021-01-14 03:43:57', '', null, '');
INSERT INTO `sys_menu` VALUES ('1111', '聚合计划新增', '1110', '1', '#', 'F', '0', 'testmanagmt:projectSuite:add', '#', 'admin', '2021-01-14 03:44:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('1112', '聚合计划编辑', '1110', '2', '#', 'F', '0', 'testmanagmt:projectSuite:edit', '#', 'admin', '2021-01-14 03:44:39', '', null, '');
INSERT INTO `sys_menu` VALUES ('1113', '聚合计划删除', '1110', '3', '#', 'F', '0', 'testmanagmt:projectSuite:remove', '#', 'admin', '2021-01-14 03:44:56', '', null, '');
INSERT INTO `sys_menu` VALUES ('1114', '聚合计划查询', '1110', '4', '#', 'F', '0', 'testmanagmt:projectSuite:list', '#', 'admin', '2021-01-14 03:45:11', '', null, '');

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(150) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1', '新版本发布啦', '2', '新版本内容', '0', 'admin', '2019-02-13 10:27:32', 'frame', '2019-02-13 10:27:32', '管理员');
INSERT INTO `sys_notice` VALUES ('2', '系统凌晨维护', '1', '维护内容', '0', 'admin', '2019-02-13 10:27:32', 'frame', '2019-02-13 10:27:32', '管理员');

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(1255) DEFAULT '' COMMENT '请求参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('100', '测试项目管理', '1', 'com.luckyframe.project.system.project.controller.ProjectController.addSave()', '1', 'admin', '研发部门', '/system/project/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"103\"],\"projectName\":[\"海军作战应急项目\"],\"deptName\":[\"研发部门\"],\"projectSign\":[\"1001\"]}', '0', null, '2022-05-13 17:42:18');
INSERT INTO `sys_oper_log` VALUES ('101', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.addSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/add', '127.0.0.1', '内网IP', '{\"moduleId\":[\"2\"],\"projectId\":[\"2\"],\"caseName\":[\"登录测试用例\"],\"caseType\":[\"1\"],\"failcontinue\":[\"0\"],\"remark\":[\"web ui 测试\"]}', '0', null, '2022-05-13 17:42:45');
INSERT INTO `sys_oper_log` VALUES ('102', '测试用例步骤管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseStepsController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseSteps/editSave', '127.0.0.1', '内网IP', '{}', '0', null, '2022-05-13 17:50:55');
INSERT INTO `sys_oper_log` VALUES ('103', '参数管理', '2', 'com.luckyframe.project.system.config.controller.ConfigController.editSave()', '1', 'admin', '研发部门', '/system/config/edit', '127.0.0.1', '内网IP', '{\"configId\":[\"1\"],\"configName\":[\"主框架页-默认皮肤样式名称\"],\"configKey\":[\"sys.index.skinName\"],\"configValue\":[\"skin-purple\"],\"configType\":[\"Y\"],\"remark\":[\"蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow\"]}', '0', null, '2022-05-13 17:56:12');
INSERT INTO `sys_oper_log` VALUES ('104', '参数管理', '2', 'com.luckyframe.project.system.config.controller.ConfigController.editSave()', '1', 'admin', '研发部门', '/system/config/edit', '127.0.0.1', '内网IP', '{\"configId\":[\"1\"],\"configName\":[\"主框架页-默认皮肤样式名称\"],\"configKey\":[\"sys.index.skinName\"],\"configValue\":[\"skin-purple\"],\"configType\":[\"Y\"],\"remark\":[\"蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow\"]}', '0', null, '2022-05-13 17:56:36');
INSERT INTO `sys_oper_log` VALUES ('105', '部门管理', '2', 'com.luckyframe.project.system.dept.controller.DeptController.editSave()', '1', 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"航天智信-总公司\"],\"orderNum\":[\"0\"],\"leader\":[\"zhangtao\"],\"phone\":[\"13311273513\"],\"email\":[\"393877568@qq.com\"],\"status\":[\"0\"]}', '0', null, '2022-05-13 18:25:17');
INSERT INTO `sys_oper_log` VALUES ('106', '客户端管理', '1', 'com.luckyframe.project.system.client.controller.ClientController.addSave()', '1', 'admin', '研发部门', '/system/client/add', '127.0.0.1', '内网IP', '{\"clientName\":[\"sztd\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"/TestDriven\"],\"remark\":[\"\"],\"status\":[\"2\"],\"clientType\":[\"0\"],\"projectIds\":[\"2\"]}', '0', null, '2022-05-13 18:25:45');
INSERT INTO `sys_oper_log` VALUES ('107', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:32:05');
INSERT INTO `sys_oper_log` VALUES ('108', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:32:41');
INSERT INTO `sys_oper_log` VALUES ('109', '客户端管理', '2', 'com.luckyframe.project.system.client.controller.ClientController.editSave()', '1', 'admin', '研发部门', '/system/client/edit', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"jobId\":[\"100\"],\"clientType\":[\"0\"],\"clientName\":[\"sztd-ui\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"],\"status\":[\"2\"],\"projectIds\":[\"2\"]}', '0', null, '2022-05-13 18:33:43');
INSERT INTO `sys_oper_log` VALUES ('110', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:21');
INSERT INTO `sys_oper_log` VALUES ('111', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:24');
INSERT INTO `sys_oper_log` VALUES ('112', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:26');
INSERT INTO `sys_oper_log` VALUES ('113', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:28');
INSERT INTO `sys_oper_log` VALUES ('114', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:30');
INSERT INTO `sys_oper_log` VALUES ('115', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:34:31');
INSERT INTO `sys_oper_log` VALUES ('116', '客户端管理', '2', 'com.luckyframe.project.system.client.controller.ClientController.editSave()', '1', 'admin', '研发部门', '/system/client/edit', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"jobId\":[\"100\"],\"clientType\":[\"0\"],\"clientName\":[\"sztd-ui\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"/TestDriven\"],\"status\":[\"2\"],\"projectIds\":[\"2\"]}', '0', null, '2022-05-13 18:37:34');
INSERT INTO `sys_oper_log` VALUES ('117', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:40:30');
INSERT INTO `sys_oper_log` VALUES ('118', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:41:17');
INSERT INTO `sys_oper_log` VALUES ('119', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:41:40');
INSERT INTO `sys_oper_log` VALUES ('120', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:42:11');
INSERT INTO `sys_oper_log` VALUES ('121', '客户端管理', '2', 'com.luckyframe.project.system.client.controller.ClientController.editSave()', '1', 'admin', '研发部门', '/system/client/edit', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"jobId\":[\"100\"],\"clientType\":[\"0\"],\"clientName\":[\"sztd-ui\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"C:/Users/MI/AppData/Local/Google/Chrome/Application/chrome.exe\"],\"status\":[\"2\"],\"projectIds\":[\"2\"]}', '0', null, '2022-05-13 18:47:43');
INSERT INTO `sys_oper_log` VALUES ('122', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:/Users/MI/AppData/Local/Google/Chrome/Application/chrome.exe\"]}', '0', null, '2022-05-13 18:52:54');
INSERT INTO `sys_oper_log` VALUES ('123', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"C:/Users/MI/AppData/Local/Google/Chrome/Application/chrome.exe\"]}', '0', null, '2022-05-13 18:52:56');
INSERT INTO `sys_oper_log` VALUES ('124', '客户端配置', '1', 'com.luckyframe.project.system.clientConfig.controller.ClientConfigController.addSave()', '1', 'admin', '研发部门', '/system/clientConfig/add', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"configKey\":[\"g\"],\"configValue\":[\"C:\\\\Users\\\\MI\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\"]}', '0', null, '2022-05-13 18:53:32');
INSERT INTO `sys_oper_log` VALUES ('125', '客户端管理', '2', 'com.luckyframe.project.system.client.controller.ClientController.editSave()', '1', 'admin', '研发部门', '/system/client/edit', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"jobId\":[\"100\"],\"clientType\":[\"0\"],\"clientName\":[\"sztd-ui\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"/TestDriven\"],\"status\":[\"2\"],\"projectIds\":[\"2,1\"]}', '0', null, '2022-05-13 18:53:48');
INSERT INTO `sys_oper_log` VALUES ('126', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:54:06');
INSERT INTO `sys_oper_log` VALUES ('127', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:57:38');
INSERT INTO `sys_oper_log` VALUES ('128', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:57:42');
INSERT INTO `sys_oper_log` VALUES ('129', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"0\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:57:50');
INSERT INTO `sys_oper_log` VALUES ('130', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 18:57:58');
INSERT INTO `sys_oper_log` VALUES ('131', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 19:03:17');
INSERT INTO `sys_oper_log` VALUES ('132', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 19:03:38');
INSERT INTO `sys_oper_log` VALUES ('133', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-13 19:06:19');
INSERT INTO `sys_oper_log` VALUES ('134', '测试用例步骤管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseStepsController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseSteps/editSave', '127.0.0.1', '内网IP', '{}', '0', null, '2022-05-15 11:11:46');
INSERT INTO `sys_oper_log` VALUES ('135', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-15 11:11:53');
INSERT INTO `sys_oper_log` VALUES ('136', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-15 11:13:29');
INSERT INTO `sys_oper_log` VALUES ('137', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-15 11:13:37');
INSERT INTO `sys_oper_log` VALUES ('138', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-15 11:15:24');
INSERT INTO `sys_oper_log` VALUES ('139', '客户端管理', '2', 'com.luckyframe.project.system.client.controller.ClientController.editSave()', '1', 'admin', '研发部门', '/system/client/edit', '127.0.0.1', '内网IP', '{\"clientId\":[\"11\"],\"jobId\":[\"100\"],\"clientType\":[\"0\"],\"clientName\":[\"sztd-ui\"],\"clientIp\":[\"127.0.0.1\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"/TestDriven;/BrowserDriven\"],\"status\":[\"2\"],\"projectIds\":[\"2,1\"]}', '0', null, '2022-05-15 11:22:54');
INSERT INTO `sys_oper_log` VALUES ('140', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:23:49');
INSERT INTO `sys_oper_log` VALUES ('141', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:28:35');
INSERT INTO `sys_oper_log` VALUES ('142', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:29:03');
INSERT INTO `sys_oper_log` VALUES ('143', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:29:07');
INSERT INTO `sys_oper_log` VALUES ('144', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"0\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:29:11');
INSERT INTO `sys_oper_log` VALUES ('145', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:29:21');
INSERT INTO `sys_oper_log` VALUES ('146', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:31:12');
INSERT INTO `sys_oper_log` VALUES ('147', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:32:59');
INSERT INTO `sys_oper_log` VALUES ('148', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:49:21');
INSERT INTO `sys_oper_log` VALUES ('149', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:54:16');
INSERT INTO `sys_oper_log` VALUES ('150', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:55:27');
INSERT INTO `sys_oper_log` VALUES ('151', '测试用例步骤管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseStepsController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseSteps/editSave', '127.0.0.1', '内网IP', '{}', '0', null, '2022-05-15 11:55:46');
INSERT INTO `sys_oper_log` VALUES ('152', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:55:52');
INSERT INTO `sys_oper_log` VALUES ('153', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:56:23');
INSERT INTO `sys_oper_log` VALUES ('154', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:57:06');
INSERT INTO `sys_oper_log` VALUES ('155', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:57:40');
INSERT INTO `sys_oper_log` VALUES ('156', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"0\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 11:58:50');
INSERT INTO `sys_oper_log` VALUES ('157', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 12:10:02');
INSERT INTO `sys_oper_log` VALUES ('158', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 13:52:28');
INSERT INTO `sys_oper_log` VALUES ('159', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:12:51');
INSERT INTO `sys_oper_log` VALUES ('160', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:15:34');
INSERT INTO `sys_oper_log` VALUES ('161', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:16:16');
INSERT INTO `sys_oper_log` VALUES ('162', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:17:26');
INSERT INTO `sys_oper_log` VALUES ('163', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:24:07');
INSERT INTO `sys_oper_log` VALUES ('164', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:24:35');
INSERT INTO `sys_oper_log` VALUES ('165', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:31:43');
INSERT INTO `sys_oper_log` VALUES ('166', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 14:58:17');
INSERT INTO `sys_oper_log` VALUES ('167', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:13');
INSERT INTO `sys_oper_log` VALUES ('168', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"0\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:19');
INSERT INTO `sys_oper_log` VALUES ('169', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"1\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:24');
INSERT INTO `sys_oper_log` VALUES ('170', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"3\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:27');
INSERT INTO `sys_oper_log` VALUES ('171', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:33');
INSERT INTO `sys_oper_log` VALUES ('172', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/TestDriven\"]}', '0', null, '2022-05-15 15:02:37');
INSERT INTO `sys_oper_log` VALUES ('173', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:02:58');
INSERT INTO `sys_oper_log` VALUES ('174', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:26:01');
INSERT INTO `sys_oper_log` VALUES ('175', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:30:46');
INSERT INTO `sys_oper_log` VALUES ('176', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:30:52');
INSERT INTO `sys_oper_log` VALUES ('177', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:36:19');
INSERT INTO `sys_oper_log` VALUES ('178', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:47:18');
INSERT INTO `sys_oper_log` VALUES ('179', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:48:07');
INSERT INTO `sys_oper_log` VALUES ('180', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:49:19');
INSERT INTO `sys_oper_log` VALUES ('181', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:50:49');
INSERT INTO `sys_oper_log` VALUES ('182', '测试用例调试(Debug)', '0', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseDebugController.debugCaseRun()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseDebug/debugCaseRun', '127.0.0.1', '内网IP', '{\"caseId\":[\"1\"],\"userId\":[\"1\"],\"caseType\":[\"1\"],\"browserType\":[\"2\"],\"clientId\":[\"11\"],\"driverPath\":[\"/BrowserDriven\"]}', '0', null, '2022-05-15 15:50:53');
INSERT INTO `sys_oper_log` VALUES ('183', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.addSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/add', '124.127.150.19', 'XX XX', '{\"moduleId\":[\"1\"],\"projectId\":[\"1\"],\"caseName\":[\"白不行\"],\"caseType\":[\"0\"],\"failcontinue\":[\"0\"],\"remark\":[\"1111\"]}', '0', null, '2022-06-19 13:30:37');
INSERT INTO `sys_oper_log` VALUES ('184', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.addSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/add', '123.117.116.121', 'XX XX', '{\"moduleId\":[\"2\"],\"projectId\":[\"2\"],\"caseName\":[\"测试新增001\"],\"caseType\":[\"0\"],\"failcontinue\":[\"0\"],\"remark\":[\"\"]}', '0', null, '2022-06-20 01:40:13');
INSERT INTO `sys_oper_log` VALUES ('185', '项目测试用例管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/edit', '123.117.116.121', 'XX XX', '{\"searchValue\":[\"\"],\"createBy\":[\"admin\"],\"createTime\":[\"2022-06-20 09:40:14\"],\"updateBy\":[\"admin\"],\"updateTime\":[\"2022-06-20 09:40:14\"],\"remark\":[\"ui系统\"],\"caseId\":[\"3\"],\"caseSerialNumber\":[\"2\"],\"caseSign\":[\"1001-2\"],\"caseName\":[\"测试新增001\"],\"projectId\":[\"2\"', '0', null, '2022-06-20 01:40:34');
INSERT INTO `sys_oper_log` VALUES ('186', '项目测试用例管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/edit', '123.117.116.121', 'XX XX', '{\"caseId\":[\"3\"],\"caseSerialNumber\":[\"2\"],\"caseSign\":[\"1001-2\"],\"projectId\":[\"2\"],\"moduleId\":[\"2\"],\"caseName\":[\"测试新增001\"],\"caseType\":[\"0\"],\"failcontinue\":[\"0\"],\"remark\":[\"ui系统01\"]}', '0', null, '2022-06-20 01:44:03');
INSERT INTO `sys_oper_log` VALUES ('187', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.copySave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/copy', '123.117.116.121', 'XX XX', '{\"caseId\":[\"3\"],\"caseSerialNumber\":[\"2\"],\"caseSign\":[\"1001-2\"],\"moduleId\":[\"2\"],\"caseIdList\":[\"\"],\"projectId\":[\"2\"],\"caseName\":[\"Copy【测试新增001】\"],\"caseType\":[\"0\"],\"failcontinue\":[\"0\"],\"remark\":[\"ui系统01\"]}', '0', null, '2022-06-20 01:44:28');
INSERT INTO `sys_oper_log` VALUES ('188', '用户管理', '2', 'com.luckyframe.project.system.user.controller.UserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '123.117.116.121', 'XX XX', '{\"userId\":[\"2\"],\"deptId\":[\"104\"],\"userName\":[\"test\"],\"email\":[\"891368884@qq.com\"],\"phonenumber\":[\"18871005653\"],\"dateQuantum\":[\"7\"],\"sex\":[\"2\"],\"projectId\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"1\"],\"postIds\":[\"3\"]}', '0', null, '2022-06-20 01:47:10');
INSERT INTO `sys_oper_log` VALUES ('189', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.copySave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/copy', '123.117.116.121', 'XX XX', '{\"caseId\":[\"2\"],\"caseSerialNumber\":[\"1\"],\"caseSign\":[\"ITT-1\"],\"moduleId\":[\"1\"],\"caseIdList\":[\"\"],\"projectId\":[\"1\"],\"caseName\":[\"Copy【白不醒】\"],\"caseType\":[\"0\"],\"failcontinue\":[\"1\"],\"remark\":[\"1111\"]}', '0', null, '2022-06-20 01:55:20');
INSERT INTO `sys_oper_log` VALUES ('190', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.batchCopy()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/batchCopy', '123.117.116.121', 'XX XX', '{\"caseId\":[\"5\"],\"caseSerialNumber\":[\"2\"],\"caseSign\":[\"ITT-2\"],\"moduleId\":[\"1\"],\"caseIdList\":[\"5,2\"],\"projectId\":[\"1\"]}', '0', null, '2022-06-20 01:55:32');
INSERT INTO `sys_oper_log` VALUES ('191', '项目测试用例管理', '3', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"7,6,5\"]}', '0', null, '2022-06-20 01:59:15');
INSERT INTO `sys_oper_log` VALUES ('192', '项目测试用例管理', '2', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/edit', '123.117.116.121', 'XX XX', '{\"caseId\":[\"2\"],\"caseSerialNumber\":[\"1\"],\"caseSign\":[\"ITT-1\"],\"projectId\":[\"1\"],\"moduleId\":[\"1\"],\"caseName\":[\"白不醒\"],\"caseType\":[\"1\"],\"failcontinue\":[\"1\"],\"remark\":[\"1111\"]}', '0', null, '2022-06-20 02:00:06');
INSERT INTO `sys_oper_log` VALUES ('193', '测试计划', '2', 'com.luckyframe.project.testmanagmt.projectPlan.controller.ProjectPlanController.editSave()', '1', 'admin', '研发部门', '/testmanagmt/projectPlan/edit', '123.117.116.121', 'XX XX', '{\"planId\":[\"1\"],\"projectId\":[\"1\"],\"planCaseCount\":[\"0\"],\"planName\":[\"白不吃你还\"],\"remark\":[\"还好吗你\"]}', '0', null, '2022-06-20 02:02:28');
INSERT INTO `sys_oper_log` VALUES ('194', '测试计划', '3', 'com.luckyframe.project.testmanagmt.projectPlan.controller.ProjectPlanController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectPlan/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"1\"]}', '0', null, '2022-06-20 02:02:31');
INSERT INTO `sys_oper_log` VALUES ('195', '聚合计划', '1', 'com.luckyframe.project.testmanagmt.projectSuite.controller.ProjectSuiteController.addSave()', '1', 'admin', '研发部门', '/testmanagmt/projectSuite/add', '123.117.116.121', 'XX XX', '{\"suitePlanCount\":[\"0\"],\"projectId\":[\"1\"],\"suiteName\":[\"1111\"],\"remark\":[\"111\"]}', '0', null, '2022-06-20 02:03:00');
INSERT INTO `sys_oper_log` VALUES ('196', '用户管理', '2', 'com.luckyframe.project.system.user.controller.UserController.editSave()', '1', 'admin', '研发部门', '/system/user/edit', '123.117.116.121', 'XX XX', '{\"userId\":[\"2\"],\"deptId\":[\"104\"],\"userName\":[\"test\"],\"email\":[\"891368884@qq.com\"],\"phonenumber\":[\"18871005653\"],\"dateQuantum\":[\"7\"],\"sex\":[\"2\"],\"projectId\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"1\"],\"postIds\":[\"3\"]}', '0', null, '2022-06-20 02:04:31');
INSERT INTO `sys_oper_log` VALUES ('197', '角色管理', '1', 'com.luckyframe.project.system.role.controller.RoleController.addSave()', '1', 'admin', '研发部门', '/system/role/add', '123.117.116.121', 'XX XX', '{\"roleName\":[\"test\"],\"roleKey\":[\"test\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"projectIds\":[\"1\"],\"menuIds\":[\"4,118,1066,1067,1068,1069,1096,1109,119,1070,1071,1072,1073,120,1074,1075,1076,1077,121,1078,1079,1080,1081,122,1082,1083,1084,1085,1110,1111,1112,1113,', '0', null, '2022-06-20 02:06:53');
INSERT INTO `sys_oper_log` VALUES ('198', '角色管理', '3', 'com.luckyframe.project.system.role.controller.RoleController.remove()', '1', 'admin', '研发部门', '/system/role/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"3\"]}', '0', null, '2022-06-20 02:08:15');
INSERT INTO `sys_oper_log` VALUES ('199', '用户管理', '3', 'com.luckyframe.project.system.user.controller.UserController.remove()', '1', 'admin', '研发部门', '/system/user/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"2\"]}', '0', null, '2022-06-20 03:03:12');
INSERT INTO `sys_oper_log` VALUES ('200', '部门管理', '1', 'com.luckyframe.project.system.dept.controller.DeptController.addSave()', '1', 'admin', '研发部门', '/system/dept/add', '123.117.116.121', 'XX XX', '{\"parentId\":[\"100\"],\"deptName\":[\"上路孤儿\"],\"orderNum\":[\"6\"],\"leader\":[\"白不醒\"],\"phone\":[\"18871005653\"],\"email\":[\"891368884@qq.com\"],\"status\":[\"0\"]}', '0', null, '2022-06-20 03:16:48');
INSERT INTO `sys_oper_log` VALUES ('201', '客户端管理', '1', 'com.luckyframe.project.system.client.controller.ClientController.addSave()', '1', 'admin', '研发部门', '/system/client/add', '123.117.116.121', 'XX XX', '{\"clientName\":[\"小鲁班\"],\"clientIp\":[\"192.168.0.102\"],\"checkinterval\":[\"30\"],\"clientPath\":[\"/TestDriven\"],\"remark\":[\"\"],\"status\":[\"2\"],\"clientType\":[\"0\"],\"projectIds\":[\"2,1\"]}', '0', null, '2022-06-20 03:23:21');
INSERT INTO `sys_oper_log` VALUES ('202', '客户端管理', '3', 'com.luckyframe.project.system.client.controller.ClientController.remove()', '1', 'admin', '研发部门', '/system/client/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"14\"]}', '0', null, '2022-06-20 03:23:33');
INSERT INTO `sys_oper_log` VALUES ('203', '客户端管理', '3', 'com.luckyframe.project.system.client.controller.ClientController.remove()', '1', 'admin', '研发部门', '/system/client/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"15\"]}', '0', null, '2022-06-20 03:24:06');
INSERT INTO `sys_oper_log` VALUES ('204', '项目测试用例管理', '3', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"4\"]}', '0', null, '2022-06-20 03:26:32');
INSERT INTO `sys_oper_log` VALUES ('205', '项目测试用例管理', '1', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.copySave()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/copy', '123.117.116.121', 'XX XX', '{\"caseId\":[\"3\"],\"caseSerialNumber\":[\"2\"],\"caseSign\":[\"1001-2\"],\"moduleId\":[\"2\"],\"caseIdList\":[\"\"],\"projectId\":[\"2\"],\"caseName\":[\"Copy【测试新增001】\"],\"caseType\":[\"0\"],\"failcontinue\":[\"0\"],\"remark\":[\"ui系统01\"]}', '0', null, '2022-06-20 03:26:42');
INSERT INTO `sys_oper_log` VALUES ('206', '岗位管理', '2', 'com.luckyframe.project.system.post.controller.PostController.editSave()', '1', 'admin', '研发部门', '/system/post/edit', '123.117.116.121', 'XX XX', '{\"postId\":[\"5\"],\"postName\":[\"上单快跑\"],\"postCode\":[\"ZM\"],\"postSort\":[\"5\"],\"status\":[\"0\"],\"remark\":[\"你猜他打的过吗\"]}', '0', null, '2022-06-20 03:28:04');
INSERT INTO `sys_oper_log` VALUES ('207', '通知公告', '1', 'com.luckyframe.project.system.notice.controller.NoticeController.addSave()', '1', 'admin', '研发部门', '/system/notice/add', '123.117.116.121', 'XX XX', '{\"noticeTitle\":[\"有问题也不确定\"],\"noticeType\":[\"2\"],\"noticeContent\":[\"<p>有的时候还好，有的不行</p>\"],\"status\":[\"0\"]}', '0', null, '2022-06-20 03:29:04');
INSERT INTO `sys_oper_log` VALUES ('208', '通知公告', '3', 'com.luckyframe.project.system.notice.controller.NoticeController.remove()', '1', 'admin', '研发部门', '/system/notice/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"3\"]}', '0', null, '2022-06-20 03:29:09');
INSERT INTO `sys_oper_log` VALUES ('209', '通知公告', '1', 'com.luckyframe.project.system.notice.controller.NoticeController.addSave()', '1', 'admin', '研发部门', '/system/notice/add', '123.117.116.121', 'XX XX', '{\"noticeTitle\":[\"有问题也不确定\"],\"noticeType\":[\"1\"],\"noticeContent\":[\"<p>有的(⊙o⊙)…</p>\"],\"status\":[\"0\"]}', '0', null, '2022-06-20 03:29:20');
INSERT INTO `sys_oper_log` VALUES ('210', '通知公告', '3', 'com.luckyframe.project.system.notice.controller.NoticeController.remove()', '1', 'admin', '研发部门', '/system/notice/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"4\"]}', '0', null, '2022-06-20 03:29:25');
INSERT INTO `sys_oper_log` VALUES ('211', '字典类型', '3', 'com.luckyframe.project.system.dict.controller.DictTypeController.remove()', '1', 'admin', '研发部门', '/system/dict/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"22\"]}', '0', null, '2022-06-20 03:40:34');
INSERT INTO `sys_oper_log` VALUES ('212', '项目测试用例管理', '3', 'com.luckyframe.project.testmanagmt.projectCase.controller.ProjectCaseController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectCase/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"2\"]}', '0', null, '2022-06-20 03:42:38');
INSERT INTO `sys_oper_log` VALUES ('213', '测试计划', '1', 'com.luckyframe.project.testmanagmt.projectPlan.controller.ProjectPlanController.addSave()', '1', 'admin', '研发部门', '/testmanagmt/projectPlan/add', '123.117.116.121', 'XX XX', '{\"planCaseCount\":[\"0\"],\"projectId\":[\"2\"],\"planName\":[\"111\"],\"remark\":[\"111111\"]}', '0', null, '2022-06-20 03:43:18');
INSERT INTO `sys_oper_log` VALUES ('214', '测试计划', '3', 'com.luckyframe.project.testmanagmt.projectPlan.controller.ProjectPlanController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectPlan/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"2\"]}', '0', null, '2022-06-20 03:43:23');
INSERT INTO `sys_oper_log` VALUES ('215', '用例公共参数', '3', 'com.luckyframe.project.testmanagmt.projectCaseParams.controller.ProjectCaseParamsController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectCaseParams/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"1\"]}', '0', null, '2022-06-20 03:43:45');
INSERT INTO `sys_oper_log` VALUES ('216', '测试计划', '3', 'com.luckyframe.project.testmanagmt.projectSuite.controller.ProjectSuiteController.remove()', '1', 'admin', '研发部门', '/testmanagmt/projectSuite/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"2\"]}', '0', null, '2022-06-20 03:44:06');
INSERT INTO `sys_oper_log` VALUES ('217', '质量管理-版本管理', '1', 'com.luckyframe.project.qualitymanagmt.qaVersion.controller.QaVersionController.addSave()', '1', 'admin', '研发部门', '/qualitymanagmt/qaVersion/add', '123.117.116.121', 'XX XX', '{\"projectId\":[\"2\"],\"versionNumber\":[\"111\"],\"versionStatus\":[\"计划中\"],\"leader\":[\"嗷嗷嗷\"],\"developer\":[\"啊啊\"],\"tester\":[\"嗷嗷\"],\"planFinishDate\":[\"2022-06-27\"],\"actuallyFinishDate\":[\"2022-06-20\"],\"launchDate\":[\"2022-06-14\"],\"timeLimitVersion\":[\"5\"],\"demandPlanFini', '0', null, '2022-06-20 03:45:43');
INSERT INTO `sys_oper_log` VALUES ('218', '质量管理-版本管理', '3', 'com.luckyframe.project.qualitymanagmt.qaVersion.controller.QaVersionController.remove()', '1', 'admin', '研发部门', '/qualitymanagmt/qaVersion/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"1\"]}', '0', null, '2022-06-20 03:45:59');
INSERT INTO `sys_oper_log` VALUES ('219', '生产事故登记', '1', 'com.luckyframe.project.qualitymanagmt.qaAccident.controller.QaAccidentController.addSave()', '1', 'admin', '研发部门', '/qualitymanagmt/qaAccident/add', '123.117.116.121', 'XX XX', '{\"projectId\":[\"2\"],\"accidentLevel\":[\"一级事故\"],\"accidentStatus\":[\"已发生-初始状态\"],\"accidentTime\":[\"2022-06-20 11:45:09\"],\"accidentType\":[\"测试人员漏测\"],\"reportTime\":[\"2022-06-19 05:25:09\"],\"accidentDescription\":[\"11\"],\"accidentAnalysis\":[\"1111\"],\"accidentConsequence\":', '0', null, '2022-06-20 03:46:55');
INSERT INTO `sys_oper_log` VALUES ('220', '生产事故登记', '3', 'com.luckyframe.project.qualitymanagmt.qaAccident.controller.QaAccidentController.remove()', '1', 'admin', '研发部门', '/qualitymanagmt/qaAccident/remove', '123.117.116.121', 'XX XX', '{\"ids\":[\"1\"]}', '0', null, '2022-06-20 03:47:07');
INSERT INTO `sys_oper_log` VALUES ('221', '客户端配置', '1', 'com.luckyframe.project.system.clientConfig.controller.ClientConfigController.addSave()', '1', 'admin', '研发部门', '/system/clientConfig/add', '123.117.116.121', 'XX XX', '{\"clientId\":[\"13\"],\"configKey\":[\"测试配置01\"],\"configValue\":[\"20\"]}', '0', null, '2022-06-20 04:03:14');

-- ----------------------------
-- Table structure for `sys_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'PM', '项目经理', '1', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '');
INSERT INTO `sys_post` VALUES ('2', 'DE', '开发工程师', '2', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '');
INSERT INTO `sys_post` VALUES ('3', 'TM', '测试经理', '3', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '');
INSERT INTO `sys_post` VALUES ('4', 'TE', '测试工程师', '4', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '');

-- ----------------------------
-- Table structure for `sys_project`
-- ----------------------------
DROP TABLE IF EXISTS `sys_project`;
CREATE TABLE `sys_project` (
  `project_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `project_name` varchar(50) NOT NULL COMMENT '项目名称',
  `dept_id` int(11) NOT NULL COMMENT '归属部门',
  `project_sign` varchar(10) NOT NULL DEFAULT 'sign' COMMENT '项目标识',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='测试项目管理表';

-- ----------------------------
-- Records of sys_project
-- ----------------------------
INSERT INTO `sys_project` VALUES ('1', '测试项目一', '104', 'ITT');
INSERT INTO `sys_project` VALUES ('2', '海军作战应急项目', '103', '1001');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', 'admin', '1', '1', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '0', '0', 'admin', '2019-02-13 10:27:32', 'admin', '2019-02-13 10:27:32', '普通角色');
INSERT INTO `sys_role` VALUES ('3', 'test', 'test', '3', '1', '0', '2', 'admin', '2022-06-20 02:06:53', '', null, null);

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `dept_id` int(11) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('2', '100');
INSERT INTO `sys_role_dept` VALUES ('2', '101');
INSERT INTO `sys_role_dept` VALUES ('2', '105');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1110');
INSERT INTO `sys_role_menu` VALUES ('1', '1111');
INSERT INTO `sys_role_menu` VALUES ('1', '1112');
INSERT INTO `sys_role_menu` VALUES ('1', '1113');
INSERT INTO `sys_role_menu` VALUES ('1', '1114');
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '105');
INSERT INTO `sys_role_menu` VALUES ('2', '106');
INSERT INTO `sys_role_menu` VALUES ('2', '107');
INSERT INTO `sys_role_menu` VALUES ('2', '108');
INSERT INTO `sys_role_menu` VALUES ('2', '109');
INSERT INTO `sys_role_menu` VALUES ('2', '110');
INSERT INTO `sys_role_menu` VALUES ('2', '111');
INSERT INTO `sys_role_menu` VALUES ('2', '112');
INSERT INTO `sys_role_menu` VALUES ('2', '113');
INSERT INTO `sys_role_menu` VALUES ('2', '114');
INSERT INTO `sys_role_menu` VALUES ('2', '115');
INSERT INTO `sys_role_menu` VALUES ('2', '116');
INSERT INTO `sys_role_menu` VALUES ('2', '500');
INSERT INTO `sys_role_menu` VALUES ('2', '501');
INSERT INTO `sys_role_menu` VALUES ('2', '1000');
INSERT INTO `sys_role_menu` VALUES ('2', '1001');
INSERT INTO `sys_role_menu` VALUES ('2', '1002');
INSERT INTO `sys_role_menu` VALUES ('2', '1003');
INSERT INTO `sys_role_menu` VALUES ('2', '1004');
INSERT INTO `sys_role_menu` VALUES ('2', '1005');
INSERT INTO `sys_role_menu` VALUES ('2', '1006');
INSERT INTO `sys_role_menu` VALUES ('2', '1007');
INSERT INTO `sys_role_menu` VALUES ('2', '1008');
INSERT INTO `sys_role_menu` VALUES ('2', '1009');
INSERT INTO `sys_role_menu` VALUES ('2', '1010');
INSERT INTO `sys_role_menu` VALUES ('2', '1011');
INSERT INTO `sys_role_menu` VALUES ('2', '1012');
INSERT INTO `sys_role_menu` VALUES ('2', '1013');
INSERT INTO `sys_role_menu` VALUES ('2', '1014');
INSERT INTO `sys_role_menu` VALUES ('2', '1015');
INSERT INTO `sys_role_menu` VALUES ('2', '1016');
INSERT INTO `sys_role_menu` VALUES ('2', '1017');
INSERT INTO `sys_role_menu` VALUES ('2', '1018');
INSERT INTO `sys_role_menu` VALUES ('2', '1019');
INSERT INTO `sys_role_menu` VALUES ('2', '1020');
INSERT INTO `sys_role_menu` VALUES ('2', '1021');
INSERT INTO `sys_role_menu` VALUES ('2', '1022');
INSERT INTO `sys_role_menu` VALUES ('2', '1023');
INSERT INTO `sys_role_menu` VALUES ('2', '1024');
INSERT INTO `sys_role_menu` VALUES ('2', '1025');
INSERT INTO `sys_role_menu` VALUES ('2', '1026');
INSERT INTO `sys_role_menu` VALUES ('2', '1027');
INSERT INTO `sys_role_menu` VALUES ('2', '1028');
INSERT INTO `sys_role_menu` VALUES ('2', '1029');
INSERT INTO `sys_role_menu` VALUES ('2', '1030');
INSERT INTO `sys_role_menu` VALUES ('2', '1031');
INSERT INTO `sys_role_menu` VALUES ('2', '1032');
INSERT INTO `sys_role_menu` VALUES ('2', '1033');
INSERT INTO `sys_role_menu` VALUES ('2', '1034');
INSERT INTO `sys_role_menu` VALUES ('2', '1035');
INSERT INTO `sys_role_menu` VALUES ('2', '1036');
INSERT INTO `sys_role_menu` VALUES ('2', '1037');
INSERT INTO `sys_role_menu` VALUES ('2', '1038');
INSERT INTO `sys_role_menu` VALUES ('2', '1039');
INSERT INTO `sys_role_menu` VALUES ('2', '1040');
INSERT INTO `sys_role_menu` VALUES ('2', '1041');
INSERT INTO `sys_role_menu` VALUES ('2', '1042');
INSERT INTO `sys_role_menu` VALUES ('2', '1043');
INSERT INTO `sys_role_menu` VALUES ('2', '1044');
INSERT INTO `sys_role_menu` VALUES ('2', '1045');
INSERT INTO `sys_role_menu` VALUES ('2', '1046');
INSERT INTO `sys_role_menu` VALUES ('2', '1047');
INSERT INTO `sys_role_menu` VALUES ('2', '1048');
INSERT INTO `sys_role_menu` VALUES ('2', '1049');
INSERT INTO `sys_role_menu` VALUES ('2', '1050');
INSERT INTO `sys_role_menu` VALUES ('2', '1051');
INSERT INTO `sys_role_menu` VALUES ('2', '1052');
INSERT INTO `sys_role_menu` VALUES ('2', '1053');
INSERT INTO `sys_role_menu` VALUES ('2', '1054');
INSERT INTO `sys_role_menu` VALUES ('2', '1055');
INSERT INTO `sys_role_menu` VALUES ('2', '1056');
INSERT INTO `sys_role_menu` VALUES ('2', '1057');
INSERT INTO `sys_role_menu` VALUES ('2', '1110');
INSERT INTO `sys_role_menu` VALUES ('2', '1111');
INSERT INTO `sys_role_menu` VALUES ('2', '1112');
INSERT INTO `sys_role_menu` VALUES ('2', '1113');
INSERT INTO `sys_role_menu` VALUES ('2', '1114');

-- ----------------------------
-- Table structure for `sys_role_project`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_project`;
CREATE TABLE `sys_role_project` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  PRIMARY KEY (`role_id`,`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和项目关联表';

-- ----------------------------
-- Records of sys_role_project
-- ----------------------------
INSERT INTO `sys_role_project` VALUES ('2', '1');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `project_id` int(8) DEFAULT NULL COMMENT '默认项目ID',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `date_quantum` int(4) DEFAULT '7' COMMENT '用户日期默认查询条件，单位：天',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', 'luckyframe', '00', 'admin@luckyframe.cn', '15888888888', '1', '', '5747f5f894ae6ed3082a1b809970e234', 'e30bbe', '0', '0', null, '192.168.1.9', '2022-06-22 22:31:43', 'admin', '2019-02-13 10:27:32', 'luckyframe', '2022-06-22 14:31:58', '管理员', '7');
INSERT INTO `sys_user` VALUES ('2', '104', 'test', 'test', '00', '891368884@qq.com', '18871005653', '2', '', 'ca7e5937a8f95a39c1c217b2eee32dc8', 'ec6e20', '0', '2', null, '123.117.116.121', '2022-06-20 10:07:16', 'admin', '2022-06-19 13:47:27', 'admin', '2022-06-20 02:07:15', '', '7');

-- ----------------------------
-- Table structure for `sys_user_online`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `post_id` int(11) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');
INSERT INTO `sys_user_post` VALUES ('2', '3');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1');

-- ----------------------------
-- Table structure for `task_case_execute`
-- ----------------------------
DROP TABLE IF EXISTS `task_case_execute`;
CREATE TABLE `task_case_execute` (
  `task_case_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '用例执行ID',
  `task_id` int(8) NOT NULL COMMENT '任务ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `case_id` int(8) NOT NULL COMMENT '用例ID',
  `case_sign` varchar(20) NOT NULL COMMENT '用例标识',
  `case_name` varchar(200) NOT NULL COMMENT '用例名称',
  `case_status` int(2) NOT NULL DEFAULT '4' COMMENT '用例执行状态 0成功 1失败 2锁定 3执行中 4未执行',
  `finish_time` datetime DEFAULT NULL COMMENT '用例结束时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `plan_id` int(11) DEFAULT NULL COMMENT '计划ID',
  PRIMARY KEY (`task_case_id`),
  KEY `task_id` (`task_id`) USING BTREE,
  KEY `case_id` (`case_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务用例执行记录';

-- ----------------------------
-- Records of task_case_execute
-- ----------------------------

-- ----------------------------
-- Table structure for `task_case_log`
-- ----------------------------
DROP TABLE IF EXISTS `task_case_log`;
CREATE TABLE `task_case_log` (
  `log_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `task_case_id` int(8) NOT NULL COMMENT '用例执行ID',
  `task_id` int(8) NOT NULL COMMENT '任务ID',
  `log_detail` varchar(5000) NOT NULL COMMENT '日志明细',
  `log_grade` varchar(20) DEFAULT NULL COMMENT '日志级别',
  `log_step` varchar(20) DEFAULT NULL COMMENT '日志用例步骤',
  `imgname` varchar(50) DEFAULT NULL COMMENT 'UI自动化自动截图地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `task_case_id` (`task_case_id`) USING BTREE,
  KEY `task_id` (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用例日志明细';

-- ----------------------------
-- Records of task_case_log
-- ----------------------------

-- ----------------------------
-- Table structure for `task_execute`
-- ----------------------------
DROP TABLE IF EXISTS `task_execute`;
CREATE TABLE `task_execute` (
  `task_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `scheduling_id` int(8) NOT NULL COMMENT '调度ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `task_name` varchar(150) NOT NULL COMMENT '任务名称',
  `task_status` int(2) DEFAULT '0' COMMENT '状态 0未执行 1执行中 2执行完成 3任务超时中断 4唤起客户端失败',
  `case_total_count` int(8) DEFAULT '0' COMMENT '总用例数',
  `case_succ_count` int(8) DEFAULT '0' COMMENT '成功数',
  `case_fail_count` int(8) DEFAULT '0' COMMENT '失败数',
  `case_lock_count` int(8) DEFAULT '0' COMMENT '锁定数',
  `case_noexec_count` int(8) DEFAULT '0' COMMENT '未执行用例',
  `finish_time` datetime DEFAULT NULL COMMENT '任务结束时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`task_id`),
  KEY `scheduling_id` (`scheduling_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试任务执行';

-- ----------------------------
-- Records of task_execute
-- ----------------------------

-- ----------------------------
-- Table structure for `task_scheduling`
-- ----------------------------
DROP TABLE IF EXISTS `task_scheduling`;
CREATE TABLE `task_scheduling` (
  `scheduling_id` int(8) NOT NULL AUTO_INCREMENT COMMENT '调度ID',
  `scheduling_name` varchar(50) NOT NULL COMMENT '调度名称',
  `job_id` int(11) NOT NULL COMMENT '调度任务ID',
  `project_id` int(8) NOT NULL COMMENT '项目ID',
  `plan_id` int(11) DEFAULT NULL,
  `client_id` int(8) NOT NULL COMMENT '客户端ID',
  `email_send_condition` int(2) DEFAULT '0' COMMENT '发送邮件通知时的具体逻辑, -1-不通知 0-全部，1-成功，2-失败',
  `email_address` varchar(300) DEFAULT NULL COMMENT '邮件通知地址',
  `building_link` varchar(200) DEFAULT NULL COMMENT 'jenkins构建链接',
  `remote_shell` varchar(200) DEFAULT NULL COMMENT '远程执行Shell脚本',
  `ex_thread_count` int(8) NOT NULL COMMENT '客户端执行线程数',
  `task_type` int(2) NOT NULL DEFAULT '0' COMMENT '任务类型 0 接口 1 Web UI 2 移动',
  `browser_type` int(2) DEFAULT NULL COMMENT 'UI自动化浏览器类型 0 IE 1 火狐 2 谷歌 3 Edge',
  `task_timeout` int(8) NOT NULL DEFAULT '60' COMMENT '任务超时时间(分钟)',
  `client_driver_path` varchar(100) DEFAULT NULL COMMENT '客户端测试驱动桩路径',
  `push_url` varchar(200) DEFAULT NULL COMMENT '第三方推送地址URL',
  `env_name` varchar(255) DEFAULT NULL COMMENT '测试环境',
  `suite_id` int(11) DEFAULT NULL COMMENT '聚合计划ID',
  `plan_type` int(11) DEFAULT '1' COMMENT '计划类型 1 单个计划 2聚合计划',
  PRIMARY KEY (`scheduling_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试任务调度';

-- ----------------------------
-- Records of task_scheduling
-- ----------------------------
