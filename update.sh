#!/bin/bash

# 拉新的镜像
msctl pull automation-frontend automation-backend # 自动化子菜单
msctl pull ui-test # UI测试子菜单
msctl pull ms-server # ms-server 主程序

# 重建容器
msctl up -d --force-recreate ui-test automation-frontend ms-server

# 清理无tag的镜像
docker images -f "dangling=true" -aq | xargs docker rmi -f

# 查看状态
msctl status
